# dune-common
# releases/2.4 # e1a9b914d0a3b133641647a6987c61c9e2a5423a # 2016-06-15 07:05:39 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
git reset --hard e1a9b914d0a3b133641647a6987c61c9e2a5423a
cd ..

# dune-istl
# releases/2.4 # ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff # 2016-07-30 12:35:06 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
git reset --hard ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff
cd ..

# dune-geometry
# releases/2.4 # ac1fca4ff249ccdc7fb035fa069853d84b93fb73 # 2016-05-28 07:30:48 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
git reset --hard ac1fca4ff249ccdc7fb035fa069853d84b93fb73
cd ..

# dune-localfunctions
# releases/2.4 # b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a # 2016-05-28 07:35:51 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
git reset --hard b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
cd ..

# dune-grid
# releases/2.4 # 5aeced8b0a64d46ff12afd3a252f99c19a8575d8 # 2016-10-11 09:17:15 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
git reset --hard 5aeced8b0a64d46ff12afd3a252f99c19a8575d8
cd ..

# dune-alugrid
# master-old # dd6058fe1e6992ee5a9d08dea3224c7b04c62dff # 2015-10-21 11:42:36 +0200 # Tobias Malkmus
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout master-old
git reset --hard dd6058fe1e6992ee5a9d08dea3224c7b04c62dff
cd ..

# dune-typetree
# releases/2.3 # ecffa10c59fa61a0071e7c788899464b0268719f # 2014-07-23 17:49:24 +0200 # Steffen Müthing
git clone https://gitlab.dune-project.org/pdelab/dune-typetree.git
cd dune-typetree
git checkout releases/2.3
git reset --hard ecffa10c59fa61a0071e7c788899464b0268719f
cd ..

# dune-pdelab
# releases/2.0 # 19c782eea7232e94849617b20dfee8d9781eb4fb # 2016-09-07 07:09:49 +0000 # Christian Engwer
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
cd dune-pdelab
git checkout releases/2.0
git reset --hard 19c782eea7232e94849617b20dfee8d9781eb4fb
cd ..

# dumux
# releases/2.9 # afd4d06c5b467575fff1dcc23f868d00c5fb2a78 # 2016-07-07 11:23:13 +0200 # Christoph Grüninger
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/2.9
git reset --hard afd4d06c5b467575fff1dcc23f868d00c5fb2a78
cd ..

# Weinhardt2016a module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Weinhardt2016a.git


