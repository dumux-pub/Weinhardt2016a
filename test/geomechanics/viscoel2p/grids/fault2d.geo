///////////////////////////////////////////////////////
// user input /////////////////////////////////////////
///////////////////////////////////////////////////////

//fault
fault_left_x = 400;
// fault_bottom_y = 0.1;
fault_right_x = 600;
// fault_top_y = 0.8;

//domain
x_min = 0.0;
x_max = 2000.0;
y_min = 0.0;
y_max = 2000.0;

fault_bottom_y = 500;
fault_top_y = 1500;

//cells in x and y direction
cells_x = 20;
cells_x_fault = 20;
cells_y = 9;
cells_z = 20;
depth = z_max; //in z-direction

///////////////////////////////////////////////////////
Point(1) = {x_min,          y_min, 0.0, 1.0};
Point(2) = {fault_left_x, y_min, 0.0, 1.0};
Point(3) = {fault_right_x,    y_min, 0.0, 1.0};
Point(4) = {x_max,          y_min, 0.0, 1.0};

Point(5) = {x_max,          fault_bottom_y, 0.0, 1.0};
Point(6) = {fault_right_x, fault_bottom_y, 0.0, 1.0};
Point(7) = {fault_left_x,    fault_bottom_y, 0.0, 1.0};
Point(8) = {x_min,          fault_bottom_y, 0.0, 1.0};

Point(9)  = {x_min,          fault_top_y, 0.0, 1.0};
Point(10) = {fault_left_x, fault_top_y, 0.0, 1.0};
Point(11) = {fault_right_x,    fault_top_y, 0.0, 1.0};
Point(12) = {x_max,          fault_top_y, 0.0, 1.0};

Point(13) = {x_max,          y_max, 0.0, 1.0};
Point(14) = {fault_right_x,    y_max, 0.0, 1.0};
Point(15) = {fault_left_x, y_max, 0.0, 1.0};
Point(16) = {x_min,          y_max, 0.0, 1.0};

//horizontal lines
Line(101) = {1, 2};
Line(102) = {2, 3};
Line(103) = {3, 4};

Line(104) = {8, 7};
Line(105) = {7, 6};
Line(106) = {6, 5};

Line(107) = {9, 10};
Line(108) = {10, 11};
Line(109) = {11, 12};

Line(110) = {16, 15};
Line(111) = {15, 14};
Line(112) = {14, 13};

//vertical lines
Line(201) = {1, 8};
Line(202) = {2, 7};
Line(203) = {3, 6};
Line(204) = {4, 5};

Line(205) = {8, 9};
Line(206) = {7, 10};
Line(207) = {6, 11};
Line(208) = {5, 12};

Line(209) = {9, 16};
Line(210) = {10, 15};
Line(211) = {11, 14};
Line(212) = {12, 13};

delta_x = x_max-x_min;

Line Loop(1) = {101, 202, -104, -201};
Plane Surface(1) = {1};
Line Loop(2) = {102, 203, -105, -202};
Plane Surface(2) = {2};
Line Loop(3) = {103, 204, -106, -203};
Plane Surface(3) = {3};

Line Loop(4) = {104, 206, -107, -205};
Plane Surface(4) = {4};
Line Loop(5) = {105, 207, -108, -206};
Plane Surface(5) = {5};
Line Loop(6) = {106, 208, -109, -207};
Plane Surface(6) = {6};

Line Loop(7) = {107, 210, -110, -209};
Plane Surface(7) = {7};
Line Loop(8) = {108, 211, -111, -210};
Plane Surface(8) = {8};
Line Loop(9) = {109, 212, -112, -211};
Plane Surface(9) = {9};

//___Horizontal lines_______
//bottom
Transfinite Line {101} = 5;
Transfinite Line {102} = 11;
Transfinite Line {103} = 15;

//fault_bottom_y
Transfinite Line {104} = 5;
Transfinite Line {105} = 11;
Transfinite Line {106} = 15;

//fault_top_y
Transfinite Line {107} = 5;
Transfinite Line {108} = 11;
Transfinite Line {109} = 15;

//top
Transfinite Line {110} = 5;
Transfinite Line {111} = 11;
Transfinite Line {112} = 15;


//___Vertical lines_______
//left
Transfinite Line {201} = 6;
Transfinite Line {205} = 51;
Transfinite Line {209} = 6;

//faut bottom
Transfinite Line {202} = 6;
Transfinite Line {206} = 51;
Transfinite Line {210} = 6;

//fault top
Transfinite Line {203} = 6;
Transfinite Line {207} = 51;
Transfinite Line {211} = 6;

//right
Transfinite Line {204} = 6;
Transfinite Line {208} = 51;
Transfinite Line {212} = 6;

Transfinite Surface {1};
Transfinite Surface {2};
Transfinite Surface {3};
Transfinite Surface {4};
Transfinite Surface {5};
Transfinite Surface {6};
Transfinite Surface {7};
Transfinite Surface {8};
Transfinite Surface {9};
Recombine Surface {1, 2, 3, 4, 5, 6, 7, 8, 9};

//Extrude top and bottom domain
// s[] = Extrude{0,y_min-fault_bottom_y,0}{Line{6,7}; Layers{(fault_bottom_y-y_min)/delta_y*cells_y}; Recombine;};
// s2[] = Extrude{0,y_max-fault_top_y,0}{Line{8,9}; Layers{(y_max-fault_top_y)/delta_y*cells_y}; Recombine;};

//Extrude one cell in depth
// Extrude{0,0,depth}{Surface{1, 2, 3, 4, 5, 6, 7, 8, 9}; Layers{cells_z}; Recombine;};

