Summary
=======

Weinhardt2016a contains a fully working version of the DuMuX code used
for his bachelor's thesis at LH2.
The required DUNE modules are listed at the end of this README.

Bibtex entry:
@MASTERSTHESIS{WeinhardtBSC2016,
  author = {Weinhardt, Felix},
  title = {Algorithmus zur Zeitschrittweitenanpassung für das linear elastische Modell},  
  school = {University of Stuttgart, Department of Hydromechanics and Modelling
	of Hydrosystems},
  year = {2016},
  institution = {University of Stuttgart, Department of Hydromechanics and Modelling
	of Hydrosystems}
}

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installWeinhardt2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Weinhardt2016a/raw/master/installWeinhardt2016a.sh)
in this folder.

```bash
mkdir -p Weinhardt2016a && cd Weinhardt2016a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Weinhardt2016a/raw/master/installWeinhardt2016a.sh
sh ./installWeinhardt2016a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  test/geomechanics/viscoel2p/test_viscoel2pRutqvist2D.cc,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  test/geomechanics/viscoel2p/test_viscoel2pRutqvist2D.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


Used Versions and Software
==========================