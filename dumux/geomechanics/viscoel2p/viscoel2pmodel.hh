// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Adaption of the fully implicit scheme to the two-phase linear elasticity model.
 */

#ifndef DUMUX_VISCOELASTIC2P_MODEL_HH
#define DUMUX_VISCOELASTIC2P_MODEL_HH

#include "viscoel2pproperties.hh"
#include "viscoel2padaptionindicator.hh"
#include <dumux/common/eigenvalues.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>

 #include <dune/pdelab/adaptivity/adaptivity.hh>

namespace Dumux {

namespace Properties {
NEW_PROP_TAG (InitialDisplacement); //!< The initial displacement function
NEW_PROP_TAG (InitialPressSat); //!< The initial pressure and saturation function
}

/*!
 * \ingroup ElTwoPBoxModel
 * \brief Adaption of the fully implicit scheme to the two-phase linear elasticity model.
 *
 * This model implements a two-phase flow of compressible immiscible fluids \f$\alpha \in \{ w, n \}\f$.
 * The deformation of the solid matrix is described with a quasi-stationary momentum balance equation.
 * The influence of the pore fluid is accounted for through the effective stress concept (Biot 1941).
 * The total stress acting on a rock is partially supported by the rock matrix and partially supported
 * by the pore fluid. The effective stress represents the share of the total stress which is supported
 * by the solid rock matrix and can be determined as a function of the strain according to Hooke's law.
 *
 * As an equation for the conservation of momentum within the fluid phases the standard multiphase Darcy's approach is used:
 \f[
 v_\alpha = - \frac{k_{r\alpha}}{\mu_\alpha} \textbf{K}
 \left(\textbf{grad}\, p_\alpha - \varrho_{\alpha} {\textbf g} \right)
 \f]
 *
 * Gravity can be enabled or disabled via the property system.
 * By inserting this into the continuity equation, one gets
 \f[
 \frac{\partial \phi_{eff} \varrho_\alpha S_\alpha}{\partial t}
 - \text{div} \left\{ \varrho_\alpha \frac{k_{r\alpha}}{\mu_\alpha}
 \mathbf{K}_\text{eff} \left(\textbf{grad}\, p_\alpha - \varrho_{\alpha} \mathbf{g} \right)
 - \phi_{eff} \varrho_\alpha S_\alpha \frac{\partial \mathbf{u}}{\partial t}
 \right\} - q_\alpha = 0 \;,
 \f]
 *
 *
 * A quasi-stationary momentum balance equation is solved for the changes with respect to the initial conditions (Darcis 2012), note
 * that this implementation assumes the soil mechanics sign convention (i.e. compressive stresses are negative):
 \f[
 \text{div}\left( \boldsymbol{\Delta \sigma'}- \Delta p_{eff} \boldsymbol{I} \right) + \Delta \varrho_b {\textbf g} = 0 \;,
 \f]
 * with the effective stress:
 \f[
 \boldsymbol{\sigma'} = 2\,G\,\boldsymbol{\epsilon} + \lambda \,\text{tr} (\boldsymbol{\epsilon}) \, \mathbf{I}.
 \f]
 *
 * and the strain tensor \f$\boldsymbol{\epsilon}\f$ as a function of the solid displacement gradient \f$\textbf{grad} \mathbf{u}\f$:
 \f[
 \boldsymbol{\epsilon} = \frac{1}{2} \, (\textbf{grad} \mathbf{u} + \textbf{grad}^T \mathbf{u}).
 \f]
 *
 * Here, the rock mechanics sign convention is switch off which means compressive stresses are < 0 and tensile stresses are > 0.
 * The rock mechanics sign convention can be switched on for the vtk output via the property system.
 *
 * The effective porosity and the effective permeability are calculated as a function of the solid displacement:
 \f[
 \phi_{eff} = \frac{\phi_{init} + \text{div} \mathbf{u}}{1 + \text{div} \mathbf{u}}
 \f]
 \f[
 K_{eff} = K_{init} \text{exp}\left( 22.2(\phi_{eff}/\phi_{init} -1 )\right)
 \f]
 * The mass balance equations are discretized using a vertex-centered finite volume (box)
 * or cell-centered finite volume scheme as spatial and the implicit Euler method as time discretization.
 * The momentum balance equations are discretized using a standard Galerkin Finite Element method as
 * spatial discretization scheme.
 *
 *
 * The primary variables are the wetting phase pressure \f$p_w\f$, the nonwetting phase saturation \f$S_n\f$ and the solid
 * displacement vector \f$\mathbf{u}\f$ (changes in solid displacement with respect to initial conditions).
 */
template<class TypeTag>
class ViscoElTwoPModel: public GET_PROP_TYPE(TypeTag, BaseModel)
{
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FVElementGeometry)) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Problem)) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, BaseModel) ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        nPhaseIdx = Indices::nPhaseIdx,
        wPhaseIdx = Indices::wPhaseIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) GridType;
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<dim>::EntityPointer DofPointer;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename Element::Geometry Geometry;
#if DUNE_VERSION_NEWER(DUNE_GRID, 2, 3)
    typedef typename Element::Geometry::JacobianInverseTransposed JacobianInverseTransposed;
#else
    typedef typename Element::Geometry::Jacobian JacobianInverseTransposed;
#endif

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::BlockVector<Dune::FieldMatrix<double, dim, dim> > TensorField;
    typedef Dune::BlockVector<Dune::FieldVector<double, dim> > VectorField;
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(SolutionVector)) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridFunctionSpace)) GridFunctionSpace;
    typedef Dune::PDELab::LocalFunctionSpace<GridFunctionSpace> LocalFunctionSpace;
    typedef typename GridType::ctype Coord;
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };

public:

    /*!
      * \brief Applies the private variables on the new grid
      */
    void updateAfterAdaption()
    {
        //this->initAfterAdaption();

        // Get new number of vertices and elements
        unsigned numVert = this->gridView().size(dim);
        unsigned numElements = this->gridView_().size(0);

        VectorField zerosNumVert(numVert, 0);
        VectorField zerosNumElements(numElements, 0);

        ScalarField zerosScalarFieldNumElements(numElements, 0);
        std::vector<Scalar> zerosVectorNumVert(numVert, 0);
        std::vector<Scalar> zerosVectorNumElements(numElements, 0);
        std::vector<bool> zerosVectorBoolNumElements(numElements, false);
        Dune::BlockVector<Dune::FieldMatrix<Scalar, dim, dim> > zerosTensorNumElements(numElements, 0);
        //Time step adaption parameters
        pCrsheBef_ = zerosVectorNumElements;
        pCrsheInit_= zerosVectorNumElements;
        //end


        Pcrshe_ = zerosVectorNumElements;
        Pcrtens_ = zerosVectorNumElements;

        willElementFail_  = zerosVectorBoolNumElements;
        hasElementFailed_ = zerosVectorBoolNumElements;
        timeStepSize_ = 0.0;

        maxIdxRow_ = zerosVectorNumElements;
        maxIdxColumn_ = zerosVectorNumElements;
        viscosity_ = zerosVectorNumElements;

        pn_= zerosVectorNumVert;
        pw_= zerosVectorNumVert;
        pc_= zerosVectorNumVert;
        sw_= zerosVectorNumVert;
        sn_= zerosVectorNumVert;

        rhoW_= zerosVectorNumVert;
        rhoN_= zerosVectorNumVert;
        Te_= zerosVectorNumVert;

        E_ = zerosVectorNumElements;
        frictionAngle_ = zerosVectorNumElements;
        cohesion_ = zerosVectorNumElements;

        effPorosity_ = zerosVectorNumElements;
        effKx_ = zerosVectorNumElements;
        effectivePressure_ = zerosVectorNumElements;
        deltaEffPressure_ = zerosVectorNumElements;

        displacement_= zerosNumVert;
        displacementTot_ = zerosNumVert;
        displacementIrr_= zerosNumVert;
        displacementIrrCumulative_= zerosNumVert;
        additionalDisplacementIrrCumulative_= zerosNumVert;
        displacementIrrCumulativeOld_ = zerosNumVert;

        divU_ = zerosVectorNumElements;
        volumetricStrain_ = zerosVectorNumElements;

        deltaEffStressX_ = zerosNumElements;
        deltaEffStressY_ = zerosNumElements;
        if (dim == 3)
            deltaEffStressZ_ = zerosNumElements;
        totalStressX_ = zerosNumElements;
        totalStressY_ = zerosNumElements;
        if (dim == 3)
            totalStressZ_ = zerosNumElements;
        initStressX_ = zerosNumElements;
        initStressY_ = zerosNumElements;
        if (dim == 3)
            initStressZ_ = zerosNumElements;
        principalStress1_ = zerosScalarFieldNumElements;
        principalStress2_ = zerosScalarFieldNumElements;
        if (dim == 3)
            principalStress3_ = zerosScalarFieldNumElements;

        cumulativeStressChangeOfFailure_ = zerosTensorNumElements;

        muMatrixElement_ = zerosTensorNumElements;
        EMatrixElement_ = zerosTensorNumElements;
        muMatrixElementLastUsedForFailure_ = zerosTensorNumElements;
        EMatrixElementLastUsedForFailure_ = zerosTensorNumElements;

    }

    Scalar getTime()
    {
        return ParentType::problem_().timeManager().time();
    }

    Scalar getTimeStepSize()
    {
        return ParentType::problem_().timeManager().timeStepSize();
    }

    const GridView& getGridView() const
    {   return this->gridView_();}

    /*!
      * \brief Serializes the current state of the model for both vertex and element
      *
      * \tparam Restarter The type of the serializer class
      *
      * \param res The serializer object
      */
     template <class Restarter>
     void serialize(Restarter &res)
     {
         res.template serializeEntities<dim>(asImp_(), this->gridView_());
         res.template serializeEntities<0>(asImp_(), this->gridView_());
     }

     /*!
      * \brief Deserializes the state of the model for both vertex and element
      *
      * \tparam Restarter The type of the serializer class
      *
      * \param res The serializer object
      */
     template <class Restarter>
     void deserialize(Restarter &res)
     {
         res.template deserializeEntities<dim>(asImp_(), this->gridView_());
         res.template deserializeEntities<0>(asImp_(), this->gridView_());

         this->prevSol() = this->curSol();
     }

     /*!
     * \brief Write the current solution to a restart file.
     *
     * \param outStream The output stream of one vertex for the restart file
     * \param entity The Entity
     *
     * Due to the mixed discretization schemes which are combined via pdelab for this model
     * the solution vector has a different form than in the pure box models
     * it sorts the primary variables in the following way:
     * p_vertex0 S_vertex0 p_vertex1 S_vertex1 p_vertex2 ....p_vertexN S_vertexN
     * ux_vertex0 uy_vertex0 uz_vertex0 ux_vertex1 uy_vertex1 uz_vertex1 ...
     *
     * Therefore, the serializeEntity function has to be modified.
     */
    template <class Entity>
    void serializeEntity(std::ostream &outStream,
            const Entity &entity)
    {
        const int codim = Entity::codimension;
        if (codim == dim) // vertex
        {
            // vertex index
            int dofIdx = this->dofMapper().index(entity);

            // write phase state
            if (!outStream.good()) {
                DUNE_THROW(Dune::IOError,
                        "Could not serialize vertex "
                        << dofIdx);
            }
            int numVert = this->gridView().size(dim);
            // get p and S entries for this vertex
            for (int eqIdx = 0; eqIdx < numEq-dim; ++eqIdx) {
                outStream << this->curSol().base()[dofIdx*(numEq-dim) + eqIdx][0]<<" ";
            }
            // get ux, uy, uz entries for this vertex
            for (int j = 0; j< dim; ++j){
                outStream << this->curSol().base()[numVert*(numEq-dim) + dofIdx*dim + j][0] <<" ";
                outStream << displacementIrrCumulative_[dofIdx][j];
            }

            int vIdx = this->dofMapper().index(entity);
            if (!outStream.good())
                DUNE_THROW(Dune::IOError, "Could not serialize vertex " << vIdx);
        }
        else { // element
            int eIdx = this->problem_().model().elementMapper().index(entity);
            for (int i = 0; i< dim; ++i)
            {
                for (int j = 0; j< dim; ++j)
                {
                    outStream << cumulativeStressChangeOfFailure_[eIdx][i][j] <<" ";
                }
                outStream << std::endl;
            }
        }
    }

    /*brief init function calls ParentType (ImplicitModel) to
     initialize GridView properly
     */
    void init(Problem &problem)
    {
        ParentType::init(problem);
        unsigned numVert = this->gridView().size(dim);
        unsigned numElements = this->gridView_().size(0);

        VectorField zerosNumVert(numVert, 0);
        VectorField zerosNumElements(numElements, 0);

        ScalarField zerosScalarFieldNumElements(numElements, 0);
        std::vector<Scalar> zerosVectorNumVert(numVert, 0);
        std::vector<Scalar> zerosVectorNumElements(numElements, 0);
        std::vector<bool> zerosVectorBoolNumElements(numElements, false);
        Dune::BlockVector<Dune::FieldMatrix<Scalar, dim, dim> > zerosTensorNumElements(numElements, 0);

        anyFailure_ = false;
        anyFailureEver_ = false;

        Pcrshe_ = zerosVectorNumElements;
        Pcrtens_ = zerosVectorNumElements;

        willElementFail_  = zerosVectorBoolNumElements;
        hasElementFailed_ = zerosVectorBoolNumElements;
        timeStepSize_ = 0.0;

        maxIdxRow_ = zerosVectorNumElements;
        maxIdxColumn_ = zerosVectorNumElements;
        viscosity_ = zerosVectorNumElements;

        pn_= zerosVectorNumVert;
        pw_= zerosVectorNumVert;
        pc_= zerosVectorNumVert;
        sw_= zerosVectorNumVert;
        sn_= zerosVectorNumVert;

        rhoW_= zerosVectorNumVert;
        rhoN_= zerosVectorNumVert;
        Te_= zerosVectorNumVert;

        E_ = zerosVectorNumElements;
        frictionAngle_ = zerosVectorNumElements;
        cohesion_ = zerosVectorNumElements;

        effPorosity_ = zerosVectorNumElements;
        effKx_ = zerosVectorNumElements;
        effectivePressure_ = zerosVectorNumElements;
        deltaEffPressure_ = zerosVectorNumElements;
        volumetricStrain_ = zerosVectorNumElements;

        displacement_= zerosNumVert;
        displacementTot_ = zerosNumVert;
        displacementIrr_= zerosNumVert;
        displacementIrrCumulative_= zerosNumVert;
        additionalDisplacementIrrCumulative_= zerosNumVert;
        displacementIrrCumulativeOld_ = zerosNumVert;

        divU_ = zerosVectorNumElements;

        deltaEffStressX_ = zerosNumElements;
        deltaEffStressY_ = zerosNumElements;
        if (dim == 3)
            deltaEffStressZ_ = zerosNumElements;
        totalStressX_ = zerosNumElements;
        totalStressY_ = zerosNumElements;
        if (dim == 3)
            totalStressZ_ = zerosNumElements;
        initStressX_ = zerosNumElements;
        initStressY_ = zerosNumElements;
        if (dim == 3)
            initStressZ_ = zerosNumElements;
        principalStress1_ = zerosScalarFieldNumElements;
        principalStress2_ = zerosScalarFieldNumElements;
        if (dim == 3)
            principalStress3_ = zerosScalarFieldNumElements;
        cumulativeStressChangeOfFailure_ = zerosTensorNumElements;

        muMatrixElement_ = zerosTensorNumElements;
        EMatrixElement_ = zerosTensorNumElements;
        muMatrixElementLastUsedForFailure_ = zerosTensorNumElements;
        EMatrixElementLastUsedForFailure_ = zerosTensorNumElements;

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndit = this->gridView_().template end<0>();

        // loop over all elements (cells)
        for (; eIt != eEndit; ++eIt) {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                // ScalarField &rank = *writer.allocateManagedBuffer(numElements);
                ScalarField rank;
                rank.resize(numElements);

                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                rank[eIdx] = this->gridView_().comm().rank();

                FVElementGeometry fvGeometry;

                //get lameParams from spatialParams and
                const Dune::FieldVector<Scalar,2> lameParams = this->problem_().spatialParams().lameParams(*eIt, fvGeometry, 0);
                //Young's modulus of the spring for the pure elastic model
                Scalar E = lameParams[0];
                //bulk modulus for both the pure elastic and the viscoelastic model
                Scalar B = lameParams[1];
                Scalar muElastic = ((3.0*B*E)/(9.0*B-E));

                // at the begin, everything is elastic
                for (int i = 0; i < dim; ++i)
                {
                    for (int j = 0; j < dim; ++j)
                    {
                        muMatrixElement_[eIdx][i][j] = muElastic;
                        EMatrixElement_[eIdx][i][j] = E;
                        muMatrixElementLastUsedForFailure_[eIdx] = muElastic;
                        EMatrixElementLastUsedForFailure_[eIdx] = E;
                    }
                }
            }
        }

    }

    /*!
     * \brief Reads the current solution for a vertex from a restart
     *        file.
     *
     * \param inStream The input stream of one vertex from the restart file
     * \param entity The Entity
     *
     * Due to the mixed discretization schemes which are combined via pdelab for this model
     * the solution vector has a different form than in the pure box models
     * it sorts the primary variables in the following way:
     * p_vertex0 S_vertex0 p_vertex1 S_vertex1 p_vertex2 ....p_vertexN S_vertexN
     * ux_vertex0 uy_vertex0 uz_vertex0 ux_vertex1 uy_vertex1 uz_vertex1 ...
     *
     * Therefore, the deserializeEntity function has to b(const SolutionVector &sol, const Entity &entity)e modified.
     */
    template<class Entity>
    void deserializeEntity(std::istream &inStream, const Entity &entity)
    {
        const int codim = Entity::codimension;
        if (codim == dim) // vertex
        {
            int dofIdx = this->dofMapper().index(entity);

            if (!inStream.good()) {
                DUNE_THROW(Dune::IOError,
                        "Could not deserialize vertex "
                        << dofIdx);
            }
            int numVert = this->gridView().size(dim);
            for (int eqIdx = 0; eqIdx < numEq-dim; ++eqIdx) {
                // read p and S entries for this vertex
                inStream >> this->curSol().base()[dofIdx*(numEq-dim) + eqIdx][0];}
            for (int j = 0; j< dim; ++j) {
                // read ux, uy, uz entries for this vertex
                inStream >> this->curSol().base()[numVert*(numEq-dim) + dofIdx*dim + j][0];
                inStream >> displacementIrrCumulative_[dofIdx][j];
            }
        }
        else { // element
            int eIdx = this->problem_().model().elementMapper().index(entity);
            for (int i = 0; i< dim; ++i)
            {
                for (int j = 0; j< dim; ++j)
                {
                    inStream >> cumulativeStressChangeOfFailure_[eIdx][i][j];
                }
            }
        }
    }

    //Function calculates Pcrshe_ from current Solution and the resulting stress field
    void evaluatePcrshe(const SolutionVector &sol)
    {

        // check whether compressive stresses are defined to be positive
        // (rockMechanicsSignConvention_ == true) or negative
        rockMechanicsSignConvention_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, RockMechanicsSignConvention);

        // create the required scalar and vector fields
        unsigned numVert = this->gridView_().size(dim);
        unsigned numElements = this->gridView_().size(0);

        VectorField zerosNumVert(numVert, 0);
        VectorField zerosNumElements(numElements, 0);

        ScalarField zerosScalarFieldNumVert(numVert, 0);
        ScalarField zerosScalarFieldNumElements(numElements, 0);
        std::vector<Scalar> zerosVectorNumElements(numElements, 0);
        Dune::BlockVector<Dune::FieldMatrix<Scalar, dim, dim> > zerosTensorNumElements(numElements, 0);

        // set to zero again, as the are cumulatively added for all SCVs (l.537ff)
        effPorosity_ = zerosVectorNumElements;
        effKx_ = zerosVectorNumElements;
        effectivePressure_ = zerosVectorNumElements;
        deltaEffPressure_ = zerosVectorNumElements;

        divU_ = zerosVectorNumElements;
        volumetricStrain_ = zerosVectorNumElements;

        VectorField displacement;
        displacement= zerosNumVert;

        VectorField NumFailedElementsWithinRange;
        NumFailedElementsWithinRange = zerosNumVert;

        VectorField deltaEffStressX;
        VectorField deltaEffStressY;
        VectorField deltaEffStressZ;
        VectorField totalStressX;
        VectorField totalStressY;
        VectorField totalStressZ;

        deltaEffStressX = zerosNumElements;
        if (dim >= 2)
            deltaEffStressY = zerosNumElements;
        if (dim == 3)
            deltaEffStressZ = zerosNumElements;

        totalStressX = zerosNumElements;
        if (dim >= 2)
            totalStressY = zerosNumElements;
        if (dim == 3)
            totalStressZ = zerosNumElements;

        ScalarField Pcrtens;
        ScalarField Pcrshe;
        ScalarField PcrsheAverage;
//         ScalarField E_viscoElement;

        Pcrtens = zerosScalarFieldNumElements;
        Pcrshe = zerosScalarFieldNumElements;
        PcrsheAverage = zerosScalarFieldNumElements;

        Scalar eps = 1.e-6;// small epsilon
        anyFailure_ = false;

        // declare and initialize start and end of vertex iterator
        VertexIterator vIt = this->gridView_().template begin<dim>();
        VertexIterator vEndIt = this->gridView_().template end<dim>();

//         ScalarField &rank = *writer.allocateManagedBuffer(numElements);
        ScalarField rank;
        rank.resize(numElements);

        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;

        const GridFunctionSpace& gridFunctionSpace = this->problem_().model().jacobianAssembler().gridFunctionSpace();
        const typename GridFunctionSpace::Ordering& ordering = gridFunctionSpace.ordering();


        // declare and initialize start and end of element iterator
        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndit = this->gridView_().template end<0>();

        // loop over all elements (cells)
        for (; eIt != eEndit; ++eIt) {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {

                // get FE function spaces to calculate gradients (gradient data of momentum balance
                // equation is not stored in fluxvars since it is not evaluated at box integration point)
                // copy the values of the sol vector to the localFunctionSpace values of the current element
                LocalFunctionSpace localFunctionSpace(gridFunctionSpace);
                localFunctionSpace.bind(*eIt);
                std::vector<Scalar> values(localFunctionSpace.size());
                for (typename LocalFunctionSpace::Traits::IndexContainer::size_type k=0; k<localFunctionSpace.size(); ++k)
                {
                    const typename GridFunctionSpace::Ordering::Traits::DOFIndex& di = localFunctionSpace.dofIndex(k);
                    typename GridFunctionSpace::Ordering::Traits::ContainerIndex ci;
                    ordering.mapIndex(di.view(),ci);
                    values[k] = sol[ci];
                }

                // local function space for pressure
                typedef typename LocalFunctionSpace::template Child<0>::Type PressSatLFS;
                const PressSatLFS& pressSatLfs = localFunctionSpace.template child<0>();
                typedef typename PressSatLFS::template Child<0>::Type PressLFS;
                const PressLFS& pressLFS = pressSatLfs.template child<0>();
                typedef typename PressLFS::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename PressLFS::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::RangeType RT_P;

                // local function space for solid displacement
                typedef typename LocalFunctionSpace::template Child<1>::Type DisplacementLFS;
                const DisplacementLFS& displacementLFS =localFunctionSpace.template child<1>();
                const unsigned int dispSize = displacementLFS.child(0).size();
                typedef typename DisplacementLFS::template Child<0>::Type ScalarDispLFS;
                // further types required for gradient calculations
                typedef typename ScalarDispLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType_V;
                typedef typename ScalarDispLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;

                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                rank[eIdx] = this->gridView_().comm().rank();

                fvGeometry.update(this->gridView_(), *eIt);
                elemVolVars.update(this->problem_(), *eIt, fvGeometry, false);

                // loop over all local vertices of the cell
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int numScv = eIt->subEntities(dim);
#else
                int numScv = eIt->template count<dim>();
#endif

                const GlobalPosition& cellCenter = eIt->geometry().center();
                const GlobalPosition& cellCenterLocal = eIt->geometry().local(cellCenter);


                std::vector<RT_P> q(pressLFS.size());
                pressLFS.finiteElement().localBasis().evaluateFunction(cellCenterLocal,q);

                // For eventual bug tracking
                if(numScv != pressLFS.size())
                    DUNE_THROW(Dumux::NumericalProblem, "Something wrong in implementation in viscoel2pmodel.hh, line 614");


                for (int scvIdx = 0; scvIdx < numScv; ++scvIdx)
                {
                    unsigned int globalIdx = this->dofMapper().subIndex(*eIt, scvIdx, dim);

                    Te_[globalIdx] = elemVolVars[scvIdx].temperature();
                    pw_[globalIdx] = elemVolVars[scvIdx].pressure(wPhaseIdx);
                    pn_[globalIdx] = elemVolVars[scvIdx].pressure(nPhaseIdx);
                    pc_[globalIdx] = elemVolVars[scvIdx].capillaryPressure();
                    sw_[globalIdx] = elemVolVars[scvIdx].saturation(wPhaseIdx);
                    sn_[globalIdx] = elemVolVars[scvIdx].saturation(nPhaseIdx);
                    rhoW_[globalIdx] = elemVolVars[scvIdx].density(wPhaseIdx);
                    rhoN_[globalIdx] = elemVolVars[scvIdx].density(nPhaseIdx);

                    // the following lines are correct for rock mechanics sign convention
                    // but lead to a very counter-intuitive output therefore, they are commented.
                    // in case of rock mechanics sign convention solid displacement is
                    // defined to be negative if it points in positive coordinate direction
    //                if(rockMechanicsSignConvention_){
    //                    DimVector tmpDispl;
    //                    tmpDispl = Scalar(0);Scalar deltaT = 1;
    //                    tmpDispl -= elemVolVars[scvIdx].displacement();
    //                    displacement[globalIdx] = tmpDispl;
    //                    }
    //
    //                else

                    displacement[globalIdx] = elemVolVars[scvIdx].displacement();

                    // The total displacement has to be calculated before the displacementIrr_ entry is updated, as the value for the timestep, that just ended,
                    // should be stored in the entry of the displacementTot.
                    displacementTot_[globalIdx] = displacement[globalIdx] + displacementIrrCumulative_[globalIdx];
                    // Same for the irreversible displacement: We want to store the old value for the vtk writer.
                    displacementIrrCumulativeOld_[globalIdx] = displacementIrrCumulative_[globalIdx];
                    double Keff;
//                     double exponent;
//                     exponent = 22.2 * (elemVolVars[scvIdx].effPorosity
//                             / elemVolVars[scvIdx].porosity() - 1);
//                     Keff = this->problem_().spatialParams().intrinsicPermeability(*eIt, fvGeometry, scvIdx)[0][0];
//                     Keff *= exp(exponent);

                    double factor;
                    factor = pow( (elemVolVars[scvIdx].effPorosity / elemVolVars[scvIdx].porosity()),15);
                    Keff = this->problem_().spatialParams().intrinsicPermeability(*eIt, fvGeometry, scvIdx)[0][0];
                    Keff *= factor;

                    effKx_[eIdx] += Keff/ numScv;

                    effectivePressure_[eIdx] += (pn_[globalIdx] * sn_[globalIdx]
                            + pw_[globalIdx] * sw_[globalIdx])
                    / numScv;

                    effPorosity_[eIdx] += elemVolVars[scvIdx].effPorosity / numScv;
                    volumetricStrain_[eIdx] += elemVolVars[scvIdx].volumetricStrain / numScv;
                    divU_[eIdx] += elemVolVars[scvIdx].divU /numScv;
                };

                deltaEffPressure_[eIdx] = effectivePressure_[eIdx] + this->problem().pInit(cellCenter, cellCenterLocal, *eIt);

                //get lameParams from spatialParams and
                const Dune::FieldVector<Scalar,2> lameParams = this->problem_().spatialParams().lameParams(*eIt, fvGeometry, 0);
                //Young's modulus of the spring for the pure elastic model
                E_[eIdx] = lameParams[0];
                //bulk modulus for both the pure elastic and the viscoelastic model
                Scalar B = lameParams[1];

                //viscosity of the damper for the Maxwell material (= viscoelastic model)
                //const Scalar viscosity = this->problem().spatialParams().viscosity(*eIt, fvGeometry, 0);

                //const GlobalPosition& cellCenter = eIt->geometry().center();
                //const GlobalPosition& cellCenterLocal = eIt->geometry().local(cellCenter);

                //deltaEffPressure_[eIdx] = effectivePressure_[eIdx] + this->problem().pInit(cellCenter, cellCenterLocal, *eIt);

                // determin changes in effective stress from current solution
                // evaluate gradient of displacement shape functions
                std::vector<JacobianType_V> vRefShapeGradient(dispSize);
                displacementLFS.child(0).finiteElement().localBasis().evaluateJacobian(cellCenterLocal, vRefShapeGradient);

                // get jacobian to transform the gradient to physical element
                const JacobianInverseTransposed jacInvT = eIt->geometry().jacobianInverseTransposed(cellCenterLocal);

                std::vector < Dune::FieldVector<RF, dim> > vShapeGradient(dispSize);
                for (size_t i = 0; i < dispSize; i++) {
                    vShapeGradient[i] = 0.0;
                    jacInvT.umv(vRefShapeGradient[i][0], vShapeGradient[i]);
                }

                // calculate gradient of current displacement
                typedef Dune::FieldMatrix<RF, dim, dim> DimMatrix;
                DimMatrix uGradient(0.0);
                for (int coordDir = 0; coordDir < dim; ++coordDir) {
                    const ScalarDispLFS & scalarDispLFS = displacementLFS.child(coordDir);

                    for (size_t i = 0; i < scalarDispLFS.size(); i++)
                    uGradient[coordDir].axpy((values[scalarDispLFS.localIndex(i)] + displacementIrr_[i][coordDir]),vShapeGradient[i]);
                    divU_[eIdx] += uGradient[coordDir][coordDir];
                }

                // calculate strain tensor
                Dune::FieldMatrix<RF, dim, dim> epsilon;

                for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                epsilon[i][j] = 0.5 * (uGradient[i][j] + uGradient[j][i]);

                // calculate constant lame Parameters
                Scalar lambda = 3.0*B*((3.0*B-E_[eIdx])/(9.0*B-E_[eIdx]));

                RF traceEpsilon = 0;
                for (int i = 0; i < dim; ++i)
                traceEpsilon += epsilon[i][i];

                // calculate effective stress tensor
                // Use muMatrixElement and not muMatrixElement_ which is used to store the reduced shear modulus for the next time the respective element fails.
                // only muMatrixElement is time-dependent, lambda is constant as it is only relevant for normal stresses
                Dune::FieldMatrix<RF, dim, dim> sigma(0.0);
                for (int i = 0; i < dim; ++i)
                {
                    sigma[i][i] = lambda * traceEpsilon;
                    for (int j = 0; j < dim; ++j)
                    {
                        // If element has failed, muMatrixElement_ contains the reduced shear modulus.
                        // If not, mu is the original mu
                        sigma[i][j] += 2.0 * muMatrixElement_[eIdx][i][j] * epsilon[i][j];
                    }
                }

                // in case of rock mechanics sign convention compressive stresses
                // are defined to be positive
                if(rockMechanicsSignConvention_)
                {
                    for (int i = 0; i < dim; ++i)
                    {
                        for (int j = 0; j < dim; ++j)
                        {
                            if (hasElementFailed_[eIdx] == true)
                            {
                                // cumulativeStressChangeOfFailure_ is not subtracted on the fault plane as reduction in this timestep was
                                // achieved via the reduced shear modulus
                                if ((i == maxIdxRow_[eIdx] && j == maxIdxColumn_[eIdx]) || (i == maxIdxColumn_[eIdx] && j == maxIdxRow_[eIdx]))
                                {
                                    if(i == 0)
                                        deltaEffStressX[eIdx][j] = -1.0 * sigma[0][j];
                                    if(i == 1)
                                        deltaEffStressY[eIdx][j] = -1.0 * sigma[1][j];
                                    if(i == 2)
                                        deltaEffStressZ[eIdx][j] = -1.0 * sigma[2][j];
                                }
                                else
                                {
                                    if(i == 0)
                                        deltaEffStressX[eIdx][j] = -1.0 * sigma[0][j] - cumulativeStressChangeOfFailure_[eIdx][0][j];
                                    if(i == 1)
                                        deltaEffStressY[eIdx][j] = -1.0 * sigma[1][j] - cumulativeStressChangeOfFailure_[eIdx][1][j];
                                    if(i == 2)
                                        deltaEffStressZ[eIdx][j] = -1.0 * sigma[2][j] - cumulativeStressChangeOfFailure_[eIdx][2][j];
                                }
                            }
                            else
                            {
                                if(i == 0)
                                    deltaEffStressX[eIdx][j] = -1.0 * sigma[0][j] - cumulativeStressChangeOfFailure_[eIdx][0][j];
                                if(i == 1)
                                    deltaEffStressY[eIdx][j] = -1.0 * sigma[1][j] - cumulativeStressChangeOfFailure_[eIdx][1][j];
                                if(i == 2)
                                    deltaEffStressZ[eIdx][j] = -1.0 * sigma[2][j] - cumulativeStressChangeOfFailure_[eIdx][2][j];
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dim; ++i)
                    {
                        for (int j = 0; j < dim; ++j)
                        {
                            if ((i == maxIdxRow_[eIdx] && j == maxIdxColumn_[eIdx]) || (i == maxIdxColumn_[eIdx] && j == maxIdxRow_[eIdx]))
                            {
                                if(i == 0)
                                    deltaEffStressX[eIdx][j] = 1.0 * sigma[0][j];
                                if(i == 1)
                                    deltaEffStressY[eIdx][j] = 1.0 * sigma[1][j];
                                if(i == 2)
                                    deltaEffStressZ[eIdx][j] = 1.0 * sigma[2][j];
                            }
                            else
                            {
                                if(i == 0)
                                    deltaEffStressX[eIdx][j] = 1.0 * sigma[0][j] + cumulativeStressChangeOfFailure_[eIdx][0][j];
                                if(i == 1)
                                    deltaEffStressY[eIdx][j] = 1.0 * sigma[1][j] + cumulativeStressChangeOfFailure_[eIdx][1][j];
                                if(i == 2)
                                    deltaEffStressZ[eIdx][j] = 1.0 * sigma[2][j] + cumulativeStressChangeOfFailure_[eIdx][2][j];
                            }
                        }
                    }
                }

                // retrieve prescribed initial stresses from problem file
                DimVector tmpInitStress = this->problem_().initialStress(cellCenter);
                if(rockMechanicsSignConvention_)
                {
                    initStressX_[eIdx][0] = tmpInitStress[0];
                    if (dim >= 2)
                    {
                        initStressY_[eIdx][1] = tmpInitStress[1];
                    }
                    if (dim >= 3)
                    {
                        initStressZ_[eIdx][2] = tmpInitStress[2];
                    }
                }
                else
                {
                    initStressX_[eIdx][0] -= tmpInitStress[0];
                    if (dim >= 2)
                    {
                        initStressY_[eIdx][1] -= tmpInitStress[1];
                    }

                    if (dim >= 3) {
                        initStressZ_[eIdx][2] -= tmpInitStress[2];
                    }
                }

                // calculate total stresses
                // in case of rock mechanics sign convention compressive stresses
                // are defined to be positive and total stress is calculated by subtracting the pore pressure
                // Since the initial stress contains the hydrostatic plus the lithostatic
                // pressure contribution, only the increment deltaEffPressure needs to be added
                if(rockMechanicsSignConvention_) {
                    totalStressX[eIdx][0] = initStressX_[eIdx][0] + deltaEffStressX[eIdx][0] - deltaEffPressure_[eIdx];
                    totalStressX[eIdx][1] = initStressX_[eIdx][1] + deltaEffStressX[eIdx][1];

                    totalStressY[eIdx][0] = initStressY_[eIdx][0] + deltaEffStressY[eIdx][0];
                    totalStressY[eIdx][1] = initStressY_[eIdx][1] + deltaEffStressY[eIdx][1] - deltaEffPressure_[eIdx];
                    if (dim >= 3)
                    {
                        totalStressX[eIdx][2] = initStressX_[eIdx][2] + deltaEffStressX[eIdx][2];

                        totalStressY[eIdx][2] = initStressY_[eIdx][2] + deltaEffStressY[eIdx][2];

                        totalStressZ[eIdx][0] = initStressZ_[eIdx][0] + deltaEffStressZ[eIdx][0];
                        totalStressZ[eIdx][1] = initStressZ_[eIdx][1] + deltaEffStressZ[eIdx][1];
                        totalStressZ[eIdx][2] = initStressZ_[eIdx][2] + deltaEffStressZ[eIdx][2] - deltaEffPressure_[eIdx];
                    }
                }
                else
                {
                    totalStressX[eIdx][0] = initStressX_[eIdx][0] + deltaEffStressX[eIdx][0] + deltaEffPressure_[eIdx];
                    totalStressX[eIdx][1] = initStressX_[eIdx][1] + deltaEffStressX[eIdx][1];

                    totalStressY[eIdx][0] = initStressY_[eIdx][0] + deltaEffStressY[eIdx][0];
                    totalStressY[eIdx][1] = initStressY_[eIdx][1] + deltaEffStressY[eIdx][1] + deltaEffPressure_[eIdx];
                    if (dim >= 3)
                    {
                        totalStressX[eIdx][2] = initStressX_[eIdx][2] + deltaEffStressX[eIdx][2];

                        totalStressY[eIdx][2] = initStressY_[eIdx][2] + deltaEffStressY[eIdx][2];

                        totalStressZ[eIdx][0] = initStressZ_[eIdx][0] + deltaEffStressZ[eIdx][0];
                        totalStressZ[eIdx][1] = initStressZ_[eIdx][1] + deltaEffStressZ[eIdx][1];
                        totalStressZ[eIdx][2] = initStressZ_[eIdx][2] + deltaEffStressZ[eIdx][2] + deltaEffPressure_[eIdx];
                    }

                }

                // calculate principal stresses i.e. the eigenvalues of the total stress tensor
                Scalar a1, a2, a3;
                DimMatrix totalStress;
                DimVector eigenValues;

                eigenValues = Scalar(0);
                totalStress = Scalar(0);

                totalStress[0] = totalStressX[eIdx];
                if (dim >= 2)
                totalStress[1] = totalStressY[eIdx];
                if (dim >= 3)
                totalStress[2] = totalStressZ[eIdx];

                calculateEigenValues<dim>(eigenValues, totalStress);

                for (int i = 0; i < dim; i++)
                {
                    if (isnan(eigenValues[i]))
                    eigenValues[i] = 0.0;
                }

                // sort principal stresses: principalStress1 >= principalStress2 >= principalStress3
                if (dim == 2) {
                    a1 = eigenValues[0];
                    a2 = eigenValues[1];

                    if (a1 >= a2) {
                        principalStress1_[eIdx] = a1;
                        principalStress2_[eIdx] = a2;
                    } else {
                        principalStress1_[eIdx] = a2;
                        principalStress2_[eIdx] = a1;
                    }
                }

                if (dim == 3) {
                    a1 = eigenValues[0];
                    a2 = eigenValues[1];
                    a3 = eigenValues[2];

                    if (a1 >= a2) {
                        if (a1 >= a3) {
                            principalStress1_[eIdx] = a1;
                            if (a2 >= a3) {
                                principalStress2_[eIdx] = a2;
                                principalStress3_[eIdx] = a3;
                            }
                            else //a3 > a2
                            {
                                principalStress2_[eIdx] = a3;
                                principalStress3_[eIdx] = a2;
                            }
                        }
                        else // a3 > a1
                        {
                            principalStress1_[eIdx] = a3;
                            principalStress2_[eIdx] = a1;
                            principalStress3_[eIdx] = a2;
                        }
                    } else // a2>a1
                    {
                        if (a2 >= a3) {
                            principalStress1_[eIdx] = a2;
                            if (a1 >= a3) {
                                principalStress2_[eIdx] = a1;
                                principalStress3_[eIdx] = a3;
                            }
                            else //a3>a1
                            {
                                principalStress2_[eIdx] = a3;
                                principalStress3_[eIdx] = a1;
                            }
                        }
                        else //a3>a2
                        {
                            principalStress1_[eIdx] = a3;
                            principalStress2_[eIdx] = a2;
                            principalStress3_[eIdx] = a1;
                        }
                    }
                }
                Scalar taum = 0.0;
                Scalar sigmam = 0.0;
                Scalar Peff = effectivePressure_[eIdx];

                const Dune::FieldVector<Scalar,2> failureCurveParams = this->problem_().spatialParams().failureCurveParams(*eIt, fvGeometry, 0);
                frictionAngle_[eIdx] = failureCurveParams[0];
                cohesion_[eIdx] = failureCurveParams[1];

                if (dim == 2)
                {
                    taum = (principalStress1_[eIdx] - principalStress2_[eIdx]) / 2;
                    sigmam = (principalStress1_[eIdx] + principalStress2_[eIdx]) / 2;
                }
                if (dim == 3)
                {
                    taum = (principalStress1_[eIdx] - principalStress3_[eIdx]) / 2;
                    sigmam = (principalStress1_[eIdx] + principalStress3_[eIdx]) / 2;
                }

                bool MohrCoulombWorstCase = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.MohrCoulombWorstCase);
                if (MohrCoulombWorstCase == true)
                {

                    Scalar Psc = -fabs(taum) / sin(frictionAngle_[eIdx]) + cohesion_[eIdx] * cos(frictionAngle_[eIdx]) / sin(frictionAngle_[eIdx]) + sigmam;
                    Pcrshe_[eIdx] = Peff - Psc;
                }
                else
                {
                    Scalar FaultAngle = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.FaultAngle);
                    Scalar sigmaFault = fabs(taum) * cos (2.0*FaultAngle * M_PI / 180.0) + sigmam;
                    Scalar tauFault = fabs(taum) * sin (2.0*FaultAngle * M_PI / 180.0);
                    Scalar sigmaFailureCurve = (tauFault - cohesion_[eIdx]) / tan(frictionAngle_[eIdx]);

                    Pcrshe_[eIdx] =  sigmaFailureCurve - sigmaFault;
                }

                // Pressure margins according to J. Rutqvist et al. / International Journal of Rock Mecahnics & Mining Sciences 45 (2008), 132-143
                if (dim == 2)
                   Pcrtens_[eIdx] = Peff - principalStress2_[eIdx];
                if (dim == 3)
                    Pcrtens_[eIdx] = Peff - principalStress3_[eIdx];


                bool initializationRun = this->problem_().initializationRun();
                if (initializationRun == false)
                {
                    //bool isElementFailed = false;
                    if ( Pcrshe_[eIdx] > eps )
                    {
                        anyFailure_ = true;
                        //Felix
                        //If Injection should stop after first Failure
                        anyFailureEver_ = true;
                        //endFelix
    //                 std::cout << "Pcrshe of Element [ " << eIdx << "] is " << Pcrshe_[eIdx] << "\n";
                    }
                }
            }
        }
        // Time Step Adaption start
        timeRefine_ = false;                //bool value from input file if time step Adaption is requestet
        timeRefine_ = GET_RUNTIME_PARAM(TypeTag, bool,TimeStepAdaption.Adaption);
        bool initializationRun = this->problem_().initializationRun();
        if (timeRefine_ == false)
        {
          changeTimeStep_ = false;
        }
        // While initialization Run all the time adaption variables get initiated
        if(initializationRun)
        {
          pCrsheBef_ = Pcrshe_;         // pressure margin for shear slip of previous time step
          pCrsheInit_ = Pcrshe_;        // initial pressure margin
          x = 0;                 // initial ratio of pressurechange
          eIt = this->gridView_().template begin<0>();
          eEndit = this->gridView_().template end<0>();
          timeStepChange_ = 1;          //value for the magnitude of time step change
          changeTimeStep_ = false;      //information about if time step change will happen
          changeMaxIndex_ = 0;          //Index of Element with highest pressure margin

          //Get information about the Time Adaption from input file
          beginAdaptionFaktor_ = GET_RUNTIME_PARAM(TypeTag, Scalar,TimeStepAdaption.BeginAdaptionFactor);
          std::cout << "beginAdaptionFaktor = " <<beginAdaptionFaktor_<<std::endl;
          changeOrder_ = GET_RUNTIME_PARAM(TypeTag, unsigned int,TimeStepAdaption.ChangeOrder);
          std::cout << "change Order = "<< changeOrder_ << std::endl;
        }
        if (!initializationRun)
        {
                eIt = this->gridView_().template begin<0>();
                eEndit = this->gridView_().template end<0>();
                timeStepChange_ = 1;
                x=0;
                pCrsheMaxBefFail_n1_ = pCrsheBef_[changeMaxIndex_];
                changeTimeStep_ = false;
            // Loop through all elements and find the maximum pressure margin
            for (; eIt != eEndit; ++eIt)
            {
              if(eIt->partitionType() == Dune::InteriorEntity)
              {
                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                //std::cout<< "p[n] = " << Pcrshe_[eIdx]<< std::endl;

                if ((Pcrshe_[eIdx]-pCrsheBef_[eIdx])/pCrsheBef_[eIdx] < x)
                {
                  x = (Pcrshe_[eIdx]-pCrsheBef_[eIdx])/pCrsheBef_[eIdx];
                  changeMaxIndex_ = eIdx;
//                   std::cout << "index Changed to "<< changeMaxIndex_<< std::endl;
                }
              }
            }
          //calculates the time step change if no element has failed
          if (anyFailure_ == false)
          {
            //condition for time step change using the bedinAdaptionFactor from input file
            if (timeRefine_ == true && Pcrshe_[changeMaxIndex_]  > pCrsheInit_[changeMaxIndex_]*beginAdaptionFaktor_)
                {
                  changeTimeStep_ = true;

                  //auxiliary variable x to calculate the final time step change
                  //x  = (Pcrshe_[changeMaxIndex_]-pCrsheBef_[changeMaxIndex_])/pCrsheBef_[changeMaxIndex_];
                  //Order of Change from 1 to 5, the higher the order, the higher the time step change
                  timeStepChange_ = pow((1+x),changeOrder_);
//                   switch (changeOrder_)
//                   {
//                     case (1) : timeStepChange_ = 1+x;
//                             std::cout << "case 1 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                             break;
//                     case (2) : timeStepChange_ = pow((1+x),cha;
//                         std::cout << "case 2 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (3) : timeStepChange_ = (1+x)*(1+x)*(1+x);
//                         std::cout << "case 3 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (4) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 4 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (5) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 5 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (6) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 5 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (7) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 5 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (8) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 5 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     case (9) : timeStepChange_ = (1+x)*(1+x)*(1+x)*(1+x)*(1+x);
//                         std::cout << "case 5 TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                     default : timeStepChange_ = 1;
//                         std::cout << "case default TimeStepCHange = "<< timeStepChange_<< std::endl;
//                         break;
//                   }
                }
          }
        }
        //time adaption end
        if (initializationRun == false)
        {
            if(anyFailure_ == true)
            {
                std::vector<bool> willElementFail(numElements, false);
                // find PcrsheMax
                Scalar PcrsheMax = 0;
                Scalar maxPcrsheIdx = 0;

                eIt = this->gridView_().template begin<0>();
                eEndit = this->gridView_().template end<0>();

                ElementPointer failedElement(eIt);

                // Loop over all elements, get global coordinates of the center
                for (; eIt != eEndit; ++eIt) {
                    if(eIt->partitionType() == Dune::InteriorEntity)
                    {

                        unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                        GlobalPosition elementCenter =  eIt->geometry().center();
//                         std::cout << "Center[" << eIdx << "] is " << elementCenter[0] << " " << elementCenter[1] << " " << elementCenter[2] << std::endl;

                        ElementIterator eItTwo = this->gridView_().template begin<0>();
                        ElementIterator eEnditTwo = this->gridView_().template end<0>();

                        Scalar sumPcrshe = 0.0;
                        int numNeighboringElements = 0;

                        // second loop over all elements, add respective Pcrshe to sumPcrshe, count numNeighboringElements within the RangeOfFailureInfluence
                        for (; eItTwo != eEnditTwo; ++eItTwo)
                        {
                            if(eIt->partitionType() == Dune::InteriorEntity)
                            {

                                GlobalPosition elementCenterTwo =  eItTwo->geometry().center();
                                Dune::FieldVector<Scalar, dimWorld> vectorBetweenElementCenters = elementCenter - elementCenterTwo;
                                Scalar distanceBetweenElementCenters = vectorBetweenElementCenters.two_norm();

                                if((distanceBetweenElementCenters < GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.RangeOfFailureInfluence) + eps)
                                    && distanceBetweenElementCenters > eps)
                                {
                                    unsigned int eIdxWithinRange = this->problem_().model().elementMapper().index(*eItTwo);

                                    sumPcrshe += Pcrshe_[eIdxWithinRange];
                                    numNeighboringElements += 1;
                                }
                            }
                        }

                        // average Pcrshe calculated from the element itself and all numNeighboringElements within the RangeOfFailureInfluence
                        PcrsheAverage[eIdx] = (Pcrshe_[eIdx]+ sumPcrshe)/(numNeighboringElements + 1);
                        if ( Pcrshe_[eIdx] > eps )
                        {
//                             std::cout << "PcrsheAverage of Element [ " << eIdx << "] is " << PcrsheAverage[eIdx] << "\n";
                            std::cout << "Pcrshe of Element [ " << eIdx << "] is " << Pcrshe_[eIdx] << "\n";
                        }

                        // find max PcrsheAverage
                        Scalar PcrsheMaxNew = fmax(PcrsheMax, PcrsheAverage[eIdx]);

                        if(PcrsheMaxNew > PcrsheMax) {
                            failedElement = eIt; // This is the failedElement
                            PcrsheMax = PcrsheMaxNew;
                            maxPcrsheIdx = eIdx;  // This is the element index of the failed Element
                        }
                    }
                }
                willElementFail[maxPcrsheIdx] = true; // vector that specifies weather the element with the index eIdx is failed or not
                std::cout << "maxPcrshe at failed Element [" << maxPcrsheIdx << "] is " << Pcrshe_[maxPcrsheIdx]<< "\n";
                willElementFail_ = willElementFail; // update the global vector
                std::cout << "willElementFail_[" << maxPcrsheIdx << "] is " << willElementFail_[maxPcrsheIdx]<< "\n";

                eIt = this->gridView_().template begin<0>();
                eEndit = this->gridView_().template end<0>();

                GlobalPosition failedElementCenter = failedElement->geometry().center();

                // another element loop within an element loop, this time for all the elements surrounding the failed element within
                // the RangeOfFailureInfluence, willElementFail_ is set to true
                for (; eIt != eEndit; ++eIt) {
                    if(eIt->partitionType() == Dune::InteriorEntity)
                    {
                        GlobalPosition elementCenter =  eIt->geometry().center();

                        Dune::FieldVector<Scalar, dimWorld> vectorBetweenElementandFailedElementCenter = elementCenter- failedElementCenter;
                        Scalar distanceBetweenElementandFailedElementCenter = vectorBetweenElementandFailedElementCenter.two_norm();

                        if((distanceBetweenElementandFailedElementCenter < GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.RangeOfFailureInfluence) + eps)
                            && distanceBetweenElementandFailedElementCenter > eps)
                        {
                            unsigned int eIdxWithinRange = this->problem_().model().elementMapper().index(*eIt);
                            willElementFail_[eIdxWithinRange] = true;
                            std::cout << "willElementFail_[" << eIdxWithinRange << "] is " << willElementFail_[eIdxWithinRange]<< "\n";
                        }
                    }
                }
            }
            // if anyFailue is false, willElementFail has to be set to false to stop failure
            else
            {
                for (unsigned int eIdx = 0; eIdx < numElements; eIdx++)
                {
                    willElementFail_[eIdx] = false;
                }
            }
        }

        eIt = this->gridView_().template begin<0>();
        eEndit = this->gridView_().template end<0>();

        // loop over all elements (cells)
        for (; eIt != eEndit; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);

                int normalDirectionOnFault = 0;
                DimVector normalVectorOnFault(0.0);

                // If the current run was done viscoelastically ,we can distinguish between
                // displacementRev and displacementIrr
                if (hasElementFailed_[eIdx] == true)
                {
                    bool initializationRun = this->problem_().initializationRun();

                    // calculate displacementIrr_ for the simulation but not for the initializationRun
                    if (initializationRun == false)
                    {

                        // Parameters accounting for the fracture opening by dilation angle
                        Scalar dilationAngle = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.DilationAngle) * M_PI / 180;

                        Dune::FieldMatrix<Scalar, dim, dim> normalEffStress;
                        normalEffStress[0][0] = deltaEffStressX_[eIdx][0] + totalStressX_[eIdx][0];
                        normalEffStress[1][1] = deltaEffStressY_[eIdx][1] + totalStressY_[eIdx][1];
                        if (dim == 3)
                            normalEffStress[2][2] = deltaEffStressZ_[eIdx][2] + totalStressZ_[eIdx][2];

                        if ( std::abs(normalEffStress[maxIdxRow_[eIdx]][maxIdxRow_[eIdx]]) > std::abs(normalEffStress[maxIdxColumn_[eIdx]][maxIdxColumn_[eIdx]]) )
                        {
                           normalDirectionOnFault = maxIdxColumn_[eIdx];
                        }
                        else
                        {
                           normalDirectionOnFault = maxIdxRow_[eIdx];
                        }
//                         int normalDirectionOnFault = dim - maxIdxRow_[eIdx] - maxIdxColumn_[eIdx];

                        normalVectorOnFault[normalDirectionOnFault] = 1.0;

                        IntersectionIterator isIt = problem().gridView().ibegin(*eIt);
                        const IntersectionIterator &isEndIt = problem().gridView().iend(*eIt);
                        for (; isIt != isEndIt; ++isIt)
                        {
                            DimVector outerNormalIntersection = isIt->centerUnitOuterNormal();

                            if (std::abs(outerNormalIntersection * normalVectorOnFault) > 0.99 )
                            {
                                int signDisplacementIrrDilation = 0;
                                if (outerNormalIntersection * normalVectorOnFault > 0)
                                {
                                    signDisplacementIrrDilation = 1;
                                }
                                else
                                    signDisplacementIrrDilation = -1;

                                const Geometry& geometry = eIt->geometry();
                                Dune::GeometryType geomType = geometry.type();
                                const ReferenceElement &referenceElement = ReferenceElements::general(geomType);

                                int fIdx = isIt->indexInInside();

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                                unsigned int numCorners = eIt->subEntities(dim)/2;
#else
                                int numCorners = eIt->template count<dim>()/2;
#endif
                                for(unsigned int i = 0; i < numCorners; i++)
                                {
                                    int vIdx = referenceElement.subEntity(fIdx, 1, i, dim);
                                    unsigned int globalIdx = this->dofMapper().subIndex(*eIt, vIdx, dim);

                                    std::cout << "eIdx is " << eIdx << "\n";
                                    //the irreversible displacement is the difference between the actual timestep and the timestep before
                                    // As the displacement increases, this calculation should give positive results
                                    // Only the displacement in direction of the maximum normal stress (of the current finished timestep)
                                    // on the fault plane is stored irreversible
                                    // As several elements can fail at the same time, the additionalDisplacementIrrCumulative_ of this timestep
                                    // is added up for all SCVs of a failed element, and then later divided by the NumFailedElementsWithinRange

                                    if ( std::abs(normalEffStress[maxIdxRow_[eIdx]][maxIdxRow_[eIdx]]) > std::abs(normalEffStress[maxIdxColumn_[eIdx]][maxIdxColumn_[eIdx]]) )
                                    {
                                        displacementIrr_[globalIdx][maxIdxRow_[eIdx]] = displacement[globalIdx][maxIdxRow_[eIdx]] - displacement_[globalIdx][maxIdxRow_[eIdx]];
                                        additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] += displacementIrr_[globalIdx][maxIdxRow_[eIdx]];
                                        NumFailedElementsWithinRange[globalIdx][maxIdxRow_[eIdx]] += 1;
//                                         std::cout << "Irreversible displacement in " << maxIdxRow_[eIdx] << std::endl;
                                        std::cout << "additionalDisplacementIrrCumulative[" << globalIdx << "] in " << maxIdxRow_[eIdx] << " is " << additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] << std::endl;

                                        // additional displacement in normal direction depending on the dilation angle
                                        displacementIrr_[globalIdx][normalDirectionOnFault] = signDisplacementIrrDilation * tan(dilationAngle) * std::abs(displacementIrr_[globalIdx][maxIdxRow_[eIdx]]);

                                        additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] += displacementIrr_[globalIdx][normalDirectionOnFault];
                                        NumFailedElementsWithinRange[globalIdx][normalDirectionOnFault] += 1;
//                                         std::cout << "Dilation in " << signDisplacementIrrDilation * normalDirectionOnFault << std::endl;
                                        std::cout << "Dilation[" << globalIdx << "] in " << signDisplacementIrrDilation * normalDirectionOnFault << " is " << additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] << std::endl;
                                    }

                                    else if ( std::abs(normalEffStress[maxIdxRow_[eIdx]][maxIdxRow_[eIdx]]) < std::abs(normalEffStress[maxIdxColumn_[eIdx]][maxIdxColumn_[eIdx]]) )
                                    {
                                        displacementIrr_[globalIdx][maxIdxColumn_[eIdx]] = displacement[globalIdx][maxIdxColumn_[eIdx]] - displacement_[globalIdx][maxIdxColumn_[eIdx]];
                                        additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] += displacementIrr_[globalIdx][maxIdxColumn_[eIdx]];
                                        NumFailedElementsWithinRange[globalIdx][maxIdxColumn_[eIdx]] += 1;
//                                         std::cout << "Irreversible displacement in " << maxIdxColumn_[eIdx] << std::endl;
                                        std::cout << "additionalDisplacementIrrCumulative[" << globalIdx << "] in " << maxIdxColumn_[eIdx] << " is " << additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] << std::endl;

                                        // additional displacement in normal direction depending on the dilation angle
                                        displacementIrr_[globalIdx][normalDirectionOnFault] = signDisplacementIrrDilation *  tan(dilationAngle) * std::abs(displacementIrr_[globalIdx][maxIdxColumn_[eIdx]]);

                                        additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] += displacementIrr_[globalIdx][normalDirectionOnFault];
                                        NumFailedElementsWithinRange[globalIdx][normalDirectionOnFault] += 1;
//                                         std::cout << "Dilation in " << signDisplacementIrrDilation * normalDirectionOnFault << std::endl;
                                        std::cout << "Dilation[" << globalIdx << "] in " << signDisplacementIrrDilation * normalDirectionOnFault << " is " << additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] << std::endl;
                                    }
                                    else
                                    {
                                        std::cout << "Normal stresses on fault plane are equal, both displacements are stored irreversible" << std::endl;

                                        displacementIrr_[globalIdx][maxIdxRow_[eIdx]] = displacement[globalIdx][maxIdxRow_[eIdx]] - displacement_[globalIdx][maxIdxRow_[eIdx]];
                                        displacementIrr_[globalIdx][maxIdxColumn_[eIdx]] = displacement[globalIdx][maxIdxColumn_[eIdx]] - displacement_[globalIdx][maxIdxColumn_[eIdx]];

                                        additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] += displacementIrr_[globalIdx][maxIdxColumn_[eIdx]];
                                        additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] += displacementIrr_[globalIdx][maxIdxRow_[eIdx]];

                                        NumFailedElementsWithinRange[globalIdx][maxIdxRow_[eIdx]] += 1;
                                        NumFailedElementsWithinRange[globalIdx][maxIdxColumn_[eIdx]] += 1;

                                        std::cout << "additionalDisplacementIrrCumulative[" << globalIdx << "] in " << maxIdxRow_[eIdx] << " is " << additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] << std::endl;
                                        std::cout << "additionalDisplacementIrrCumulative[" << globalIdx << "] in " << maxIdxColumn_[eIdx] << " is " << additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] << std::endl;

                                        // additional displacement in normal direction depending on the dilation angle - in this case depending on the net displacement (=Resultierende)
                                        displacementIrr_[globalIdx][normalDirectionOnFault] = signDisplacementIrrDilation * tan(dilationAngle) * sqrt( pow(displacementIrr_[globalIdx][maxIdxRow_[eIdx]], 2.0) +
                                        pow(displacementIrr_[globalIdx][maxIdxColumn_[eIdx]], 2.0) );

                                        additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] += displacementIrr_[globalIdx][normalDirectionOnFault];
                                        NumFailedElementsWithinRange[globalIdx][normalDirectionOnFault] += 1;
//                                         std::cout << "Dilation in " << signDisplacementIrrDilation * normalDirectionOnFault << std::endl;
                                        std::cout << "Dilation[" << globalIdx << "] in " << signDisplacementIrrDilation * normalDirectionOnFault << " is " << additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] << std::endl;
                                    }
                                }
                            }
                        }
                    }
                }

// #if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
//                 int numScv = eIt->subEntities(dim);
// #else
//                 int numScv = eIt->template count<dim>();
// #endif
//                 // first loop over all subcontrolvolumes (nodes)
//                 // displacementIrrCumulative_ is calculated
//                 for (int scvIdx = 0; scvIdx < numScv; ++scvIdx)

                IntersectionIterator isIt = problem().gridView().ibegin(*eIt);
                const IntersectionIterator &isEndIt = problem().gridView().iend(*eIt);
                for (; isIt != isEndIt; ++isIt)
                {
//                     unsigned int globalIdx = this->dofMapper().map(*eIt, scvIdx, dim);

                    if (hasElementFailed_[eIdx] == true)
                    {
                        bool initializationRun = this->problem_().initializationRun();

                        // calculate displacementIrr_ for the simulation but not for the initializationRun a
                        if (initializationRun == false)
                        {
                            // Again the direction of the maximum normal stress (of the current finished timestep) is evaluated, and the new displacementIrrCumulative_
                            // is calculated from additionalDisplacementIrrCumulative_ / NumFailedElementsWithinRange
                            DimVector outerNormalIntersection = isIt->centerUnitOuterNormal();

                            if (std::abs(outerNormalIntersection * normalVectorOnFault) > 0.99 )
                            {

                                const Geometry& geometry = eIt->geometry();
                                Dune::GeometryType geomType = geometry.type();
                                const ReferenceElement &referenceElement = ReferenceElements::general(geomType);

                                int fIdx = isIt->indexInInside();

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                                unsigned int numCorners = eIt->subEntities(dim)/2;
#else
                                int numCorners = eIt->template count<dim>()/2;
#endif
                                for(unsigned int i = 0; i < numCorners; i++)
                                {
                                    int vIdx = referenceElement.subEntity(fIdx, 1, i, dim);
                                    unsigned int globalIdx = this->dofMapper().subIndex(*eIt, vIdx, dim);                                   // normal stress for figuring out in which direction slip happens. cumulativeStressChange not relevant for normal stresses
                                    Dune::FieldMatrix<Scalar, dim, dim> normalEffStress;
                                    normalEffStress[0][0] = deltaEffStressX_[eIdx][0] + totalStressX_[eIdx][0];
                                    normalEffStress[1][1] = deltaEffStressY_[eIdx][1] + totalStressY_[eIdx][1];
                                    if (dim == 3)
                                        normalEffStress[2][2] = deltaEffStressZ_[eIdx][2] + totalStressZ_[eIdx][2];

                                    if ( std::abs(normalEffStress[maxIdxRow_[eIdx]][maxIdxRow_[eIdx]]) > std::abs(normalEffStress[maxIdxColumn_[eIdx]][maxIdxColumn_[eIdx]]) )
                                    {
                                        displacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] += (additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] / NumFailedElementsWithinRange[globalIdx][maxIdxRow_[eIdx]]);
                                    }

                                    else if ( std::abs(normalEffStress[maxIdxRow_[eIdx]][maxIdxRow_[eIdx]]) < std::abs(normalEffStress[maxIdxColumn_[eIdx]][maxIdxColumn_[eIdx]]) )
                                    {
                                        displacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] += (additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] / NumFailedElementsWithinRange[globalIdx][maxIdxColumn_[eIdx]]);
                                    }
                                    else
                                    {
                                        displacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] += (additionalDisplacementIrrCumulative_[globalIdx][maxIdxRow_[eIdx]] / NumFailedElementsWithinRange[globalIdx][maxIdxRow_[eIdx]]);
                                        displacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] += (additionalDisplacementIrrCumulative_[globalIdx][maxIdxColumn_[eIdx]] / NumFailedElementsWithinRange[globalIdx][maxIdxColumn_[eIdx]]);
                                    }

                                    // For the contribution of the dilation to displacementIrrCumulative_, the determination of the direction via the maximum normal stress is not necessary, as the normal direction on the fault plane is unambiguous
                                    displacementIrrCumulative_[globalIdx][normalDirectionOnFault] += (additionalDisplacementIrrCumulative_[globalIdx][normalDirectionOnFault] / NumFailedElementsWithinRange[globalIdx][normalDirectionOnFault]);
                                }
                            }
                        }
                    }
                }

                // if failure has happened
                if (hasElementFailed_[eIdx] == true)
                {
                    // For a failed element, the stressChange is stored and added up to make the stress relief at that element permanent
                    // but only for the shear stress acting on the actual fault plane
                    Dune::FieldMatrix<Scalar, dim, dim> stressChange;
                    stressChange = 0.0;

                    // Old shear stress > new shear stress, thus stress change should be positive
                    if (maxIdxRow_[eIdx] == 0)
                    {
                        stressChange[maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = deltaEffStressX_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressX[eIdx][maxIdxColumn_[eIdx]];
                        stressChange[maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = deltaEffStressX_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressX[eIdx][maxIdxColumn_[eIdx]];
                    }

                    else if (maxIdxRow_[eIdx] == 1)
                    {
                        stressChange[maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = deltaEffStressY_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressY[eIdx][maxIdxColumn_[eIdx]];
                        stressChange[maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = deltaEffStressY_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressY[eIdx][maxIdxColumn_[eIdx]];
                    }
                    else if (maxIdxRow_[eIdx] == 2)
                    {
                        stressChange[maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = deltaEffStressZ_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressZ[eIdx][maxIdxColumn_[eIdx]];
                        stressChange[maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = deltaEffStressZ_[eIdx][maxIdxColumn_[eIdx]] - deltaEffStressZ[eIdx][maxIdxColumn_[eIdx]];
                    }

                    else
                      std::cout << "WARNING: Index out of range \n";

/*                        for (int j = 0; j < dim; ++j)
                        {
                            cumulativeStressChangeOfFailure_[eIdx] += stressChange;

                        }    */
//                    }

                    // the positive stress difference is added up
                    cumulativeStressChangeOfFailure_[eIdx] += stressChange;

                     std::cout << "eIdx is " << eIdx << "\n";
                     if (dim ==2)
                     {
                        std::cout << "stressChange is " << stressChange[0][0] << " " << stressChange[0][1] << "\n";
                        std::cout << "stressChange is " << stressChange[1][0] << " " << stressChange[1][1] << "\n";

                        std::cout << "cumulativeStressChangeOfFailure_ is " << cumulativeStressChangeOfFailure_[eIdx][0][0] << " " << cumulativeStressChangeOfFailure_[eIdx][0][1] << "\n";
                        std::cout << "cumulativeStressChangeOfFailure_ is " << cumulativeStressChangeOfFailure_[eIdx][1][0] << " " << cumulativeStressChangeOfFailure_[eIdx][1][1] << "\n";
                     }
                     if (dim ==3)
                     {
                        std::cout << "stressChange is " << stressChange[0][0] << " " << stressChange[0][1] << " " << stressChange[0][2] << "\n";
                        std::cout << "stressChange is " << stressChange[1][0] << " " << stressChange[1][1] << " " << stressChange[1][2] << "\n";
                        std::cout << "stressChange is " << stressChange[2][0] << " " << stressChange[2][1] << " " << stressChange[2][2] << "\n";

                        std::cout << "cumulativeStressChangeOfFailure_ is " << cumulativeStressChangeOfFailure_[eIdx][0][0] << " " << cumulativeStressChangeOfFailure_[eIdx][0][1] << " " << cumulativeStressChangeOfFailure_[eIdx][0][2] << "\n";
                        std::cout << "cumulativeStressChangeOfFailure_ is " << cumulativeStressChangeOfFailure_[eIdx][1][0] << " " << cumulativeStressChangeOfFailure_[eIdx][1][1] << " " << cumulativeStressChangeOfFailure_[eIdx][1][2] << "\n";
                        std::cout << "cumulativeStressChangeOfFailure_ is " << cumulativeStressChangeOfFailure_[eIdx][2][0] << " " << cumulativeStressChangeOfFailure_[eIdx][2][1] << " " << cumulativeStressChangeOfFailure_[eIdx][2][2] << "\n";
                     }

                }

                // update the deltaEffStress
                deltaEffStressX_[eIdx] = deltaEffStressX[eIdx];
                deltaEffStressY_[eIdx] = deltaEffStressY[eIdx];
                if (dim == 3)
                    deltaEffStressZ_[eIdx] = deltaEffStressZ[eIdx];
                totalStressX_[eIdx] = totalStressX[eIdx];
                totalStressY_[eIdx] = totalStressY[eIdx];
                if (dim == 3)
                    totalStressZ_[eIdx] = totalStressZ[eIdx];
            }
        }

        // initialize start and end of vertex iterator
        vIt = this->gridView_().template begin<dim>();
        vEndIt = this->gridView_().template end<dim>();

        //TODO: Remove unnecessary commented lines. Can loop over elements and SCVs be replaced by loop over vertices?
        eIt = this->gridView_().template begin<0>();
        eEndit = this->gridView_().template end<0>();

        // loop over all elements (cells)
        for (; eIt != eEndit; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {

                // loop over all subcontrolvolumes (nodes)
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int numScv = eIt->subEntities(dim);
#else
                int numScv = eIt->template count<dim>();
#endif

                for (int scvIdx = 0; scvIdx < numScv; ++scvIdx)
                {
                            unsigned int globalIdx = this->dofMapper().subIndex(*eIt, scvIdx, dim);
//                     bool initializationRun = this->problem_().initializationRun();


                    //if (initializationRun == false)
                    //{
                        // update global displacement_
                        for (int i = 0; i < dim; ++i)
                        {
                            displacement_[globalIdx][i] = displacement[globalIdx][i];

                        // update local displacementIrr_
//                            // if this is the first timestep of a new failure process (failureDurationElement_[eIdx] = deltaT)
//                            if ( (failureDurationElement_[eIdx] < deltaT + eps) && (failureDurationElement_[eIdx] > -eps) )
//                            {
//                                displacementIrr_[globalIdx][i] += displacementIrr[globalIdx][i];
//                            }
//                            // if failureDurationElement_[eIdx] > deltaT or failureDurationElement_[eIdx] < deltaT
//                            else
//                            {
//                                displacementIrr_[globalIdx][i] = displacementIrr[globalIdx][i];
//                            }
                        }
                   // }
                }
            }
        }

    }

    // Before each timestep, evaluateFailureParameters function is called
    void evaluateFailureParameters()
    {
        // check whether compressive stresses are defined to be positive
        // (rockMechanicsSignConvention_ == true) or negative
        rockMechanicsSignConvention_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, RockMechanicsSignConvention);

        // create the required scalar and vector fields
        unsigned numVert = this->gridView_().size(dim);
        unsigned numElements = this->gridView_().size(0);

        VectorField zerosNumVert(numVert, 0);
        VectorField zerosNumElements(numElements, 0);
        std::vector<bool> zerosVectorBoolNumElements(numElements, false);
        ScalarField zerosScalarFieldNumElements(numElements, 0);

        // create the required fields for vertex data
        ScalarField SumTimesteps;
        SumTimesteps = zerosScalarFieldNumElements;
        timeStepSize_ = 0.0;

        Dune::BlockVector<Dune::FieldMatrix<Scalar, dim, dim> > zerosTensorNumElements(numElements, 0);

        Scalar timeStepSize = getTimeStepSize();
        hasElementFailed_ = zerosVectorBoolNumElements;

        ScalarField rank;
        rank.resize(numElements);

        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;

        // declare and initialize start and end of element iterator
        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndit = this->gridView_().template end<0>();


        //loop over all elements
        for (; eIt != eEndit; ++eIt) {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                rank[eIdx] = this->gridView_().comm().rank();

                fvGeometry.update(this->gridView_(), *eIt);
                elemVolVars.update(this->problem_(), *eIt, fvGeometry, false);
                //get lameParams from spatialParams and
                const Dune::FieldVector<Scalar,2> lameParams = this->problem_().spatialParams().lameParams(*eIt, fvGeometry, 0);
//                 //Young's modulus of the spring for the pure elastic model
                Scalar E = lameParams[0];
                //bulk modulus for both the pure elastic and the viscoelastic model
                Scalar B = lameParams[1];

                // mu and E tu be used in current are set to the original values for each cell, and only the mu and E of failing cells
                // is changed -> see below
                for(int i = 0; i < dim; ++i)
                {
                    for(int j = 0; j < dim; ++j)
                    {
                        EMatrixElement_[eIdx][i][j] = E;
                        muMatrixElement_[eIdx][i][j] = (3 * B * E) / (9 * B - E);
                    }
                }

                const Dune::FieldVector<Scalar,2> viscoParams = this->problem_().spatialParams().viscoParams(*eIt, fvGeometry, 0);
                // Scalar viscosity_constant = viscoParams[0];
                Scalar deltaSigma = viscoParams[1];

                // Indices for the maximum shear strain
                int maxIdxRow = 0;
                int maxIdxColumn = 0;

                Scalar totalStressSheMaxTemp = 0.0;
                Dune::FieldMatrix<Scalar, dim, dim> totalStressMatrixFailedElement(0);

                // for failed element
                if (willElementFail_[eIdx] == true)
                {

                    hasElementFailed_[eIdx] = true;

                    std::cout << "hasElementFailed_[" << eIdx << "] set to " << willElementFail_[eIdx]<< "\n";
                    //assemble stress matrix of previous timestep from totalStressX_[eIdx], totalStressY_[eIdx], totalStressZ_[eIdx]
                    for(int i = 0; i < dim; ++i)
                    {
                        totalStressMatrixFailedElement[i][0] = totalStressX_[eIdx][i];
                        totalStressMatrixFailedElement[i][1] = totalStressY_[eIdx][i];
                        if (dim == 3)
                            totalStressMatrixFailedElement[i][2] = totalStressZ_[eIdx][i];
                    }

//                    // for failed element
//                    if (willElementFail_[eIdx] == true)
//                    {
//                        std::cout << "\n";
//                        std::cout << "total Stress of Element " << eIdx << " is \n";
//                        std::cout << totalStressMatrixFailedElement[0][0] << " " << totalStressMatrixFailedElement[1][0] << " " << totalStressMatrixFailedElement[2][0] << "\n";
//                        std::cout << totalStressMatrixFailedElement[0][1] << " " << totalStressMatrixFailedElement[1][1] << " " << totalStressMatrixFailedElement[2][1] << "\n";
//                        std::cout << totalStressMatrixFailedElement[0][2] << " " << totalStressMatrixFailedElement[1][2] << " " << totalStressMatrixFailedElement[2][2] << "\n";
//                        std::cout << "\n";
//                    }

                    // find max shear stress and the direction of max shear stress
                    for(int i = 0; i < dim; ++i)
                    {
                        for(int j = 0; j < dim; ++j)
                        {
                            if (i != j)
                            {
                                Scalar totalStressSheMaxTempNew = fmax(totalStressSheMaxTemp, std::abs(totalStressMatrixFailedElement[i][j]));
                                if(totalStressSheMaxTempNew > totalStressSheMaxTemp) {
                                    maxIdxRow = i;
                                    maxIdxColumn = j;
                                }
                                totalStressSheMaxTemp = totalStressSheMaxTempNew;
                                //std::cout << "epsilon[" << i << "],[" << j << "] at element " << eIdx << " is " << std::abs (epsilon[i][j]) << "\n";
                            }
                        }
                    }

                    Scalar deltaSigmaCorrected = 0.0;

                    if (totalStressSheMaxTemp > deltaSigma)
                    {
                        deltaSigmaCorrected = deltaSigma;
                    }
                    else
                    {
                        deltaSigmaCorrected = 0.50 * totalStressSheMaxTemp;
                        std::cout << "WARNING: The stress drop deltaSigma is larger than totalStressSheMaxTemp, thus 50 percent of the totalStressSheMaxTemp is used for the stress drop" << std::endl;
                    }

                    Scalar viscosity;

                    Scalar ratioStresses = (totalStressSheMaxTemp - deltaSigmaCorrected)/(totalStressSheMaxTemp);

                    // Old Young's modulus and shear modulus. EMatrixElement_[eIdx] and muMatrixElement_[eIdx] are symmetric, so it
                    // does not matter if [maxIdxRow][maxIdxColumn] or [maxIdxColumn][maxIdxRow] is used.
//                             Scalar EOld = E;
                    Scalar EOld  = EMatrixElementLastUsedForFailure_[eIdx][maxIdxRow][maxIdxColumn];
                    Scalar muOld = muMatrixElementLastUsedForFailure_[eIdx][maxIdxRow][maxIdxColumn];
                    std::cout << "muOld at element " << eIdx << " is " << muOld << "\n";
                    std::cout << "EOld at element " << eIdx << " is " << EOld << "\n";

                    // New Young's modulus from reduction in the shear modulus mu, which depends on ratioStresses
                    Scalar ENew = (9 * B * ratioStresses * muOld) / (3 * B + ratioStresses * muOld);
                    std::cout << "ENew at element " << eIdx << " is " << ENew << "\n";

                    Scalar ratioENewEOld = ENew/EOld;
                    viscosity = (-timeStepSize*EOld)/(std::log(ratioENewEOld));
                    std::cout << "Viscosity at element " << eIdx << " is " << viscosity << "\n";

                    Scalar muNew = (3 * B * ENew) / (9 * B - ENew);
                    std::cout << "muNew at element " << eIdx << " is " << muNew << "\n";

                    std::cout << "ratioStresses is " << ratioStresses << std::endl;
                    std::cout << "ratio mu is " << muNew / muOld << std::endl;

                    //std::cout << "maxIdxRowPostTimeStep = " << maxIdxRowPostTimeStep << " maxIdxColumnPostTimeStep = " << maxIdxColumnPostTimeStep << " at element " << eIdx <<  "\n";

                    // update the respective entry of the failed element in the global viscosity vector
                    if (viscosity > 0)
                    {
                        viscosity_[eIdx] = viscosity;
                    }

                    // update new value for the orientation of the maximal shear stress for the respective element
                    maxIdxRow_[eIdx] = maxIdxRow;
                    maxIdxColumn_[eIdx] = maxIdxColumn;

                    //            std::cout << "E " << eIdx << " is " << E << "\n";
                                std::cout << "totalStressSheMaxTemp at element " << eIdx << " is " << totalStressSheMaxTemp << "\n";
                                std::cout << "deltaSigmaCorrected " << eIdx << " is " << deltaSigmaCorrected << "\n";
                    //            std::cout << "ln(" << (totalStressSheMaxTemp - deltaSigma)/(totalStressSheMaxTemp) << ")\n";
                    //            std::cout << "deltaT " << eIdx << " is " << deltaT << "\n";
                                std::cout << "Viscosity_ at element " << eIdx << " is " << viscosity_[eIdx] << "\n";
                    //            std::cout << "maxIdxRow now at element " << eIdx << " is " << maxIdxRow << " and maxIdxColumn is " << maxIdxColumn << "\n";
                    //            std::cout << "maxIdxRow old at element " << eIdx << " is " << maxIdxRow_[eIdx] << " and maxIdxColumn is " << maxIdxColumn_[eIdx] << "\n";

                    // mu and E of failing cells are set tu the new values for the upcoming timestep
                    EMatrixElement_[eIdx][maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = ENew;
                    EMatrixElement_[eIdx][maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = ENew;

                    muMatrixElement_[eIdx][maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = muNew;
                    muMatrixElement_[eIdx][maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = muNew;

                    // new mu and E are stored and will be used the next time the element fails als Eold
                    EMatrixElementLastUsedForFailure_[eIdx][maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = ENew;
                    EMatrixElementLastUsedForFailure_[eIdx][maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = ENew;

                    muMatrixElementLastUsedForFailure_[eIdx][maxIdxRow_[eIdx]][maxIdxColumn_[eIdx]] = muNew;
                    muMatrixElementLastUsedForFailure_[eIdx][maxIdxColumn_[eIdx]][maxIdxRow_[eIdx]] = muNew;

                }
            }
        }
    }

    /*!
     * \brief \copybrief ImplicitModel::addOutputVtkFields
     *
     * Specialization for the ElOnePTwoCBoxModel, add one-phase two-component
     * properties, solid displacement, stresses, effective properties and the
     * process rank to the VTK writer.
     * No calculation, only writing out results. Unless stated otherwise, variables
     * are calculated in the evaluatePcrshe function and this fact is illustrated by
     * naming these variables "PostTimeStep.
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol, MultiWriter &writer) {
        // check whether compressive stresses are defined to be positive
        // (rockMechanicsSignConvention_ == true) or negative
        rockMechanicsSignConvention_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, RockMechanicsSignConvention);

        // create the required scalar and vector fields
        unsigned numVert = this->gridView_().size(dim);
        unsigned numElements = this->gridView_().size(0);

        // create the required fields for vertex data
        VectorField &displacementPostTimeStep = *writer.template allocateManagedBuffer<Scalar, dim>(numVert);
        VectorField &displacementIrrCumulativePostTimeStep = *writer.template allocateManagedBuffer<Scalar, dim>(numVert);
        VectorField &displacementTotPostTimeStep = *writer.template allocateManagedBuffer<Scalar, dim>(numVert);

        ScalarField &divUPostTimeStep = *writer.allocateManagedBuffer(numElements);

        ScalarField &E = *writer.allocateManagedBuffer(numElements);
        ScalarField &viscosity = *writer.allocateManagedBuffer(numElements);
        ScalarField &volumetricStrain = *writer.allocateManagedBuffer(numElements);
        ScalarField &frictionAngle = *writer.allocateManagedBuffer(numElements);
        ScalarField &cohesion = *writer.allocateManagedBuffer(numElements);

        ScalarField &pnPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &pwPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &pcPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &swPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &snPostTimeStep = *writer.allocateManagedBuffer(numVert);

        ScalarField &pInit = *writer.allocateManagedBuffer(numVert);
        ScalarField &pwOld = *writer.allocateManagedBuffer(numVert);
        ScalarField &ElementLevel = *writer.allocateManagedBuffer(numElements);

        ScalarField &rhoWPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &rhoNPostTimeStep = *writer.allocateManagedBuffer(numVert);
        ScalarField &TePostTimeStep = *writer.allocateManagedBuffer(numVert);

        // create the required fields for element data
        // effective stresses
        VectorField &deltaEffStressXPostTimeStep = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);
        VectorField &deltaEffStressYPostTimeStep = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);
        VectorField &deltaEffStressZPostTimeStep = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);
        // total stresses
        VectorField &totalStressXPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        VectorField &totalStressYPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        VectorField &totalStressZPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        // initial stresses
        VectorField &initStressXPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        VectorField &initStressYPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        VectorField &initStressZPostTimeStep = *writer.template allocateManagedBuffer<
        Scalar, dim>(numElements);
        // principal stresses
        ScalarField &principalStress1PostTimeStep = *writer.allocateManagedBuffer(
                numElements);
        ScalarField &principalStress2PostTimeStep = *writer.allocateManagedBuffer(
                numElements);
        ScalarField &principalStress3PostTimeStep = *writer.allocateManagedBuffer(
                numElements);

        VectorField &cumulativeStressChangeOfFailureX = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);
        VectorField &cumulativeStressChangeOfFailureY = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);
        VectorField &cumulativeStressChangeOfFailureZ = *writer.template allocateManagedBuffer<Scalar,
        dim>(numElements);

        ScalarField &effKxPostTimeStep = *writer.allocateManagedBuffer(numElements);
        ScalarField &effPorosityPostTimeStep = *writer.allocateManagedBuffer(numElements);
        ScalarField &effectivePressurePostTimeStep = *writer.allocateManagedBuffer(numElements);
        ScalarField &deltaEffPressurePostTimeStep = *writer.allocateManagedBuffer(numElements);
        ScalarField &PcrtensPostTimeStep = *writer.allocateManagedBuffer(numElements);
        ScalarField &PcrshePostTimeStep = *writer.allocateManagedBuffer(numElements);
//        ScalarField &E_viscoElement = *writer.allocateManagedBuffer(numElements);

        // declare and initialize start and end of vertex iterator
        VertexIterator vIt = this->gridView_().template begin<dim>();
        VertexIterator vEndIt = this->gridView_().template end<dim>();

        // set local variables to global variables_ s that should be printed out
        for(; vIt != vEndIt; ++vIt)
        {
            int globalIdx = this->problem_().vertexMapper().index(*vIt);

            pnPostTimeStep[globalIdx] = pn_[globalIdx];
            pwPostTimeStep[globalIdx] = pw_[globalIdx];
            pcPostTimeStep[globalIdx] = pc_[globalIdx];
            swPostTimeStep[globalIdx] = sw_[globalIdx];
            snPostTimeStep[globalIdx] = sn_[globalIdx];

            pInit[globalIdx] = this->problem_().pInit()[globalIdx];
            pwOld[globalIdx] = this->problem_().model().prevSol().base()[2*globalIdx][0];

            rhoWPostTimeStep[globalIdx] = rhoW_[globalIdx];
            rhoNPostTimeStep[globalIdx] = rhoN_[globalIdx];
            TePostTimeStep[globalIdx] = Te_[globalIdx];

            displacementPostTimeStep[globalIdx] = displacement_[globalIdx];
            displacementIrrCumulativePostTimeStep[globalIdx] = displacementIrrCumulativeOld_[globalIdx];
            displacementTotPostTimeStep[globalIdx] = displacementTot_[globalIdx];
        }

        ScalarField &rank = *writer.allocateManagedBuffer(numElements);

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndit = this->gridView_().template end<0>();

        // loop over all elements (cells)
        for (; eIt != eEndit; ++eIt) {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                unsigned int eIdx = this->problem_().model().elementMapper().index(*eIt);
                rank[eIdx] = this->gridView_().comm().rank();

                PcrshePostTimeStep[eIdx] = Pcrshe_[eIdx];
                PcrtensPostTimeStep[eIdx] = Pcrtens_[eIdx];
                effKxPostTimeStep[eIdx] = effKx_[eIdx];
                effPorosityPostTimeStep[eIdx] = effPorosity_[eIdx];
                effectivePressurePostTimeStep[eIdx] = effectivePressure_[eIdx];
                deltaEffPressurePostTimeStep[eIdx] = deltaEffPressure_[eIdx];

                principalStress1PostTimeStep[eIdx] = principalStress1_[eIdx];
                principalStress2PostTimeStep[eIdx] = principalStress2_[eIdx];
                if (dim == 3)
                    principalStress3PostTimeStep[eIdx] = principalStress3_[eIdx];
                deltaEffStressXPostTimeStep[eIdx] = deltaEffStressX_[eIdx];
                deltaEffStressYPostTimeStep[eIdx] = deltaEffStressY_[eIdx];
                if (dim == 3)
                    deltaEffStressZPostTimeStep[eIdx] = deltaEffStressZ_[eIdx];
                totalStressXPostTimeStep[eIdx] = totalStressX_[eIdx];
                totalStressYPostTimeStep[eIdx] = totalStressY_[eIdx];
                if (dim == 3)
                    totalStressZPostTimeStep[eIdx] = totalStressZ_[eIdx];
                initStressXPostTimeStep[eIdx] = initStressX_[eIdx];
                initStressYPostTimeStep[eIdx] = initStressY_[eIdx];
                if (dim == 3)
                    initStressZPostTimeStep[eIdx] = initStressZ_[eIdx];

                cumulativeStressChangeOfFailureX[eIdx] = cumulativeStressChangeOfFailure_[eIdx][0];
                cumulativeStressChangeOfFailureY[eIdx] = cumulativeStressChangeOfFailure_[eIdx][1];
                if (dim == 3)
                    cumulativeStressChangeOfFailureZ[eIdx] = cumulativeStressChangeOfFailure_[eIdx][2];

                divUPostTimeStep[eIdx] = divU_[eIdx];
                volumetricStrain[eIdx] = volumetricStrain_[eIdx];

                E[eIdx] = E_[eIdx];
                viscosity[eIdx] = viscosity_[eIdx];
                frictionAngle[eIdx] = frictionAngle_[eIdx];
                cohesion[eIdx] = cohesion_[eIdx];

                ElementLevel[eIdx] = eIt->level();
            }
        }
        //Output file for a better comparability of time adaption
        bool initializationRun = this->problem_().initializationRun();
        if(initializationRun == false)
        {
        f3.open("StepSizeAll.dat", std::ios::app);
        f3 << getTime() <<" "<< pCrsheBef_[changeMaxIndex_] << " "<< getTimeStepSize() << " "<< changeMaxIndex_ << " " << timeStepChange_<<"\n";
        f3.close();
        //upgrades pCrshe with Pcrshe
        pCrsheBef_ = Pcrshe_;
        }

        writer.attachVertexData(TePostTimeStep, "T");
        writer.attachVertexData(pwPostTimeStep, "pw");
        writer.attachVertexData(pnPostTimeStep, "pN");
        writer.attachVertexData(pcPostTimeStep, "pC");
        writer.attachVertexData(swPostTimeStep, "SW");
        writer.attachVertexData(snPostTimeStep, "SN");
        writer.attachVertexData(rhoWPostTimeStep, "rhoW");
        writer.attachVertexData(rhoNPostTimeStep, "rhoN");

        writer.attachVertexData(pInit, "pInit");
        writer.attachVertexData(pwOld, "pWOld");
        writer.attachCellData(ElementLevel, "GridLevel");

        writer.attachVertexData(displacementPostTimeStep, "u", dim);writer.attachVertexData(displacementIrrCumulativePostTimeStep, "uirr_cumulative", dim);writer.attachVertexData(displacementTotPostTimeStep, "utot", dim);

        writer.attachCellData(divUPostTimeStep, "uDiv");
        writer.attachCellData(volumetricStrain, "volumetricStrain");

        writer.attachCellData(E, "E");
        writer.attachCellData(viscosity, "viscosity");
        writer.attachCellData(frictionAngle, "frictionAngle");
        writer.attachCellData(cohesion, "cohesion");

        writer.attachCellData(deltaEffStressXPostTimeStep, "effectiveStressChangesX", dim);
        if (dim >= 2)
        writer.attachCellData(deltaEffStressYPostTimeStep, "effectiveStressChangesY", dim);
        if (dim >= 3)
        writer.attachCellData(deltaEffStressZPostTimeStep, "effectiveStressChangesZ", dim);

        writer.attachCellData(principalStress1PostTimeStep, "principal stress 1");
        if (dim >= 2)
        writer.attachCellData(principalStress2PostTimeStep, "principal stress 2");
        if (dim >= 3)
        writer.attachCellData(principalStress3PostTimeStep, "principal stress 3");

        writer.attachCellData(totalStressXPostTimeStep, "total stresses X", dim);
        if (dim >= 2)
        writer.attachCellData(totalStressYPostTimeStep, "total stresses Y", dim);
        if (dim >= 3)
        writer.attachCellData(totalStressZPostTimeStep, "total stresses Z", dim);

        writer.attachCellData(initStressXPostTimeStep, "initial stresses X", dim);
        if (dim >= 2)
        writer.attachCellData(initStressYPostTimeStep, "initial stresses Y", dim);
        if (dim >= 3)
        writer.attachCellData(initStressZPostTimeStep, "initial stresses Z", dim);

        writer.attachCellData(cumulativeStressChangeOfFailureX, "cumulativeStressChangesX", dim);
        if (dim >= 2)
        writer.attachCellData(cumulativeStressChangeOfFailureY, "cumulativeStressChangesY", dim);
        if (dim >= 3)
        writer.attachCellData(cumulativeStressChangeOfFailureZ, "cumulativeStressChangesZ", dim);

        writer.attachCellData(deltaEffPressurePostTimeStep, "delta pEff");
        writer.attachCellData(effectivePressurePostTimeStep, "effectivePressure");
        writer.attachCellData(PcrtensPostTimeStep, "Pcr_tensile");
        writer.attachCellData(PcrshePostTimeStep, "Pcrshe");
        writer.attachCellData(effKxPostTimeStep, "effective Kxx");
        writer.attachCellData(effPorosityPostTimeStep, "effective Porosity");
    }

    /*!
     * \brief Applies the initial solution for all vertices of the grid.
     */
    void applyInitialSolution_() {
        typedef typename GET_PROP_TYPE(TypeTag, PTAG(InitialPressSat)) InitialPressSat;
        InitialPressSat initialPressSat(this->problem_().gridView());
        std::cout << "viscoel2pmodel calls: initialPressSat" << std::endl;
        initialPressSat.setPressure(this->problem_().pInit());

        typedef typename GET_PROP_TYPE(TypeTag, PTAG(InitialDisplacement)) InitialDisplacement;
        InitialDisplacement initialDisplacement(this->problem_().gridView());

        typedef Dune::PDELab::CompositeGridFunction<InitialPressSat,
        InitialDisplacement> InitialSolution;
        InitialSolution initialSolution(initialPressSat, initialDisplacement);

        int numDofs = this->jacobianAssembler().gridFunctionSpace().size();

        std::cout << "numDofs = " << numDofs << std::endl;

        Dune::PDELab::interpolate(initialSolution,
                this->jacobianAssembler().gridFunctionSpace(), this->curSol());
        Dune::PDELab::interpolate(initialSolution,
                this->jacobianAssembler().gridFunctionSpace(), this->prevSol());

        //set once during initialization
        firstassymetricFailure_ = false;
        eIdxWithProblem_ = 0;

    }

    const Problem& problem() const {
        return this->problem_();
    }

public:
    Dune::FieldVector<Scalar, dim> displacementIrrCumulative(unsigned int globalIdx) const
    {
        return displacementIrrCumulative_[globalIdx];
    }

    Dune::FieldVector<Scalar, dim> displacementBefore(unsigned int globalIdx) const
    {
        return displacement_[globalIdx];
    }

    Scalar maxIdxRow(unsigned int eIdx) const //Element &element
    {
        return maxIdxRow_[eIdx];
    }

    Scalar maxIdxColumn(unsigned int eIdx) const //Element &element
    {
        return maxIdxColumn_[eIdx];
    }

    Dune::FieldVector<Scalar, dim> totalStressX(unsigned int eIdx) const //Element &element
    {
        return totalStressX_[eIdx];
    }

    Dune::FieldVector<Scalar, dim> totalStressY(unsigned int eIdx) const //Element &element
    {
        return totalStressY_[eIdx];
    }

    Dune::FieldVector<Scalar, dim> totalStressZ(unsigned int eIdx) const //Element &element
    {
        return totalStressZ_[eIdx];
    }

    Scalar viscosity(unsigned int eIdx) const //Element &element
    {
        return viscosity_[eIdx];
    }

    Dune::FieldMatrix<Scalar, dim, dim> cumulativeStressChangeOfFailure(unsigned int eIdx) const
    {
        return cumulativeStressChangeOfFailure_[eIdx];
    }

    Dune::FieldMatrix<Scalar, dim, dim> muMatrixElement(unsigned int eIdx) const//Element &element
    {
        return muMatrixElement_[eIdx];
    }

    bool willElementFail(unsigned int eIdx) const//Element &element
    {
        return willElementFail_[eIdx];
    }

    bool anyFailure() const
    {
        return anyFailure_;
    }

    bool anyFailureEver() const
    {
        return anyFailureEver_;
    }

    Scalar pcrShear(int idx) const
    {
      return Pcrshe_[idx];
    }

    //time step adaption parameters
    Scalar timeStepChange()
    {
      return timeStepChange_;
    }
    unsigned int changeMaxIndex()
    {
      return changeMaxIndex_;
    }
    Scalar PcrsheChangeMax()
    {
      return Pcrshe_[changeMaxIndex_];
    }
    bool ChangeTimeStep ()
    {
      return changeTimeStep_;
    }
    //time step adaption parameters end
private:

    //time step Adaption Parameters
    bool timeRefine_;
    std::vector<Scalar> pCrsheBef_;
    std::vector<Scalar> pCrsheInit_;

    std::fstream f3;
    Scalar pCrsheMaxBefFail_n1_;
    Scalar x;
    Scalar timeStepChange_;
    unsigned int changeMaxIndex_;
    Scalar pCrshMaxInit_;
    bool changeTimeStep_;
    Scalar beginAdaptionFaktor_;
    unsigned int changeOrder_;
    //time step Adaption Parameters End


    bool rockMechanicsSignConvention_;
    bool anyFailure_;
    bool anyFailureEver_;
    std::vector<bool> willElementFail_;
    std::vector<bool> hasElementFailed_;
    bool firstassymetricFailure_;
    std::vector<Scalar> maxIdxRow_;
    std::vector<Scalar> maxIdxColumn_;
    Scalar timeStepSize_;
    std::vector<Scalar> Pcrshe_;
    int eIdxWithProblem_;

    std::vector<Scalar> pn_;
    std::vector<Scalar> pw_;
    std::vector<Scalar> pc_;
    std::vector<Scalar> sw_;
    std::vector<Scalar> sn_;
    VectorField displacement_;
    VectorField displacementIrr_;
    VectorField displacementTot_;
    VectorField displacementIrrCumulative_;
    VectorField additionalDisplacementIrrCumulative_;
    VectorField displacementIrrCumulativeOld_;
    std::vector<Scalar> rhoW_;
    std::vector<Scalar> rhoN_;
    std::vector<Scalar> Te_;

    std::vector<Scalar> E_;
    TensorField EMatrixElement_;
    TensorField muMatrixElement_;
    TensorField EMatrixElementLastUsedForFailure_;
    TensorField muMatrixElementLastUsedForFailure_;

    std::vector<Scalar> frictionAngle_;
    std::vector<Scalar> cohesion_;

    std::vector<Scalar> divU_;
    std::vector<Scalar> volumetricStrain_;

    VectorField deltaEffStressX_;
    VectorField deltaEffStressY_;
    VectorField deltaEffStressZ_;
    VectorField totalStressX_;
    VectorField totalStressY_;
    VectorField totalStressZ_;
    VectorField initStressX_;
    VectorField initStressY_;
    VectorField initStressZ_;
    ScalarField principalStress1_;
    ScalarField principalStress2_;
    ScalarField principalStress3_;

    Dune::BlockVector<Dune::FieldMatrix<Scalar, dim, dim>> cumulativeStressChangeOfFailure_;

    std::vector<Scalar> effKx_;
    std::vector<Scalar> effPorosity_;
    std::vector<Scalar> effectivePressure_;
    std::vector<Scalar> deltaEffPressure_;

    std::vector<Scalar> viscosity_;
    std::vector<Scalar> Pcrtens_;

    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

};
}
#include "viscoel2ppropertydefaults.hh"
#endif
