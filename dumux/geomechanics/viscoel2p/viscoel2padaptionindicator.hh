// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef VISCOEL2P_ADAPTION_INDICATOR_HH
#define VISCOEL2P_ADAPTION_INDICATOR_HH

#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/geometry/quadraturerules.hh>


namespace Dumux {

namespace PDELab {


template<typename TypeTag>
class ViscoEl2pAdaptionIndicator
  : public Dune::PDELab::LocalOperatorDefaultFlags
{
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

private:
  Problem& problem_;
  Scalar tolerance_;

public:

  // pattern assembly flags
  enum { doPatternVolume = false };
  enum { doPatternSkeleton = false };

  // residual assembly flags
  enum { doAlphaVolume  = true };

  //! constructor: pass parameter object
  ViscoEl2pAdaptionIndicator (Problem& problem)
    : problem_(problem)
  {
    tolerance_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Adaption.Tolerance);
  }

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    // extract local function space
    typedef typename LFSU::template Child<0>::Type PressSatLFS;
    typedef typename PressSatLFS::template Child<0>::Type PressLFS;

    // domain and range field type
    typedef typename PressLFS::Traits::FiniteElementType::
                    Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename PressLFS::Traits::FiniteElementType::
                    Traits::LocalBasisType::Traits::DomainFieldType DF;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int intorder = 1;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>& rule =
      Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    RF sum(0.0);
    for (typename Dune::QuadratureRule<DF,dim>::const_iterator
         it=rule.begin(); it!=rule.end(); ++it)
    {
      // get global idx of the element
      int eIdx = problem_.model().elementMapper().map(eg.entity());
      // get pcrshear value for element
      RF pcrshear = problem_.model().pcrShear(eIdx);

      RF difference = pcrshear - tolerance_;

      RF value = difference > 0 && pcrshear < 0 ? difference/std::abs(tolerance_) : 0;

      // geometric weight needed for quadrature rule evaluation (numerical integration)
      RF qWeight = it->weight() * eg.geometry().integrationElement(it->position());

      sum += value;
    }

    // accumulate cell indicator
    r.accumulate(lfsv, 0, sum);
  }
};

}
}

#endif // VISCOEL2P_ADAPTION_INDICATOR_HH
