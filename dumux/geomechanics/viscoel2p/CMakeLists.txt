
install(FILES
        viscoel2padaptionindicator.hh
        viscoel2passembler.hh
        viscoel2pelementvolumevariables.hh
        viscoel2pfluxvariables.hh
        viscoel2pindices.hh
        viscoel2plocaljacobian.hh
        viscoel2plocaloperator.hh
        viscoel2plocalresidual.hh
        viscoel2pmodel.hh
        viscoel2pnewtoncontroller.hh
        viscoel2pproperties.hh
        viscoel2ppropertydefaults.hh
        viscoel2pvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/geomechanics/viscoel2p)
